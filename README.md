# A selection of code samples and templates
## Table of Contents
- [A Dummy Shopping Cart (gRPC project sample, written in GO)](./poisk/README.md)
- [gRPC sample code, written in GO](./pirs/README.md)
- [Cookiecutter project for aws api gateway + golang lambda](./rassvet/README.md)
- [Cookiecutter project for gRPC Service](./oktan/README.md)
