# Serverless-golang {{cookiecutter.app_name}} service
Serverless boilerplate code for golang with GET and POST 

There are three endpoint provided:
1. GET endpoint with name parameter (/{{cookiecutter.app_name}}/{name}). Example: curl https://uaowl5rxh6.execute-api.us-east-1.amazonaws.com/dev/{{cookiecutter.app_name}}/shirt
2. GET endpoint with query string parameter ({{cookiecutter.app_name}}?name=$name). Example: curl https://uaowl5rxh6.execute-api.us-east-1.amazonaws.com/dev/{{cookiecutter.app_name}}?name=shirt
3. POST endpoint with name in the body (/{{cookiecutter.app_name}} - with JSON body {"name":$name}. Example: curl --request POST 'https://uaowl5rxh6.execute-api.us-east-1.amazonaws.com/dev/{{cookiecutter.app_name}}' --header 'Content-Type: application/json' --data-raw '{"name": "shirt"}'
