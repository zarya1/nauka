// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package v1

import (
	context "context"
	wrappers "github.com/golang/protobuf/ptypes/wrappers"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// ShoppingCartClient is the client API for ShoppingCart service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type ShoppingCartClient interface {
	AddItem(ctx context.Context, in *Item, opts ...grpc.CallOption) (*wrappers.StringValue, error)
	GetItem(ctx context.Context, in *wrappers.StringValue, opts ...grpc.CallOption) (*Item, error)
}

type shoppingCartClient struct {
	cc grpc.ClientConnInterface
}

func NewShoppingCartClient(cc grpc.ClientConnInterface) ShoppingCartClient {
	return &shoppingCartClient{cc}
}

func (c *shoppingCartClient) AddItem(ctx context.Context, in *Item, opts ...grpc.CallOption) (*wrappers.StringValue, error) {
	out := new(wrappers.StringValue)
	err := c.cc.Invoke(ctx, "/ecommerce.ShoppingCart/addItem", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *shoppingCartClient) GetItem(ctx context.Context, in *wrappers.StringValue, opts ...grpc.CallOption) (*Item, error) {
	out := new(Item)
	err := c.cc.Invoke(ctx, "/ecommerce.ShoppingCart/getItem", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ShoppingCartServer is the server API for ShoppingCart service.
// All implementations must embed UnimplementedShoppingCartServer
// for forward compatibility
type ShoppingCartServer interface {
	AddItem(context.Context, *Item) (*wrappers.StringValue, error)
	GetItem(context.Context, *wrappers.StringValue) (*Item, error)
	mustEmbedUnimplementedShoppingCartServer()
}

// UnimplementedShoppingCartServer must be embedded to have forward compatible implementations.
type UnimplementedShoppingCartServer struct {
}

func (UnimplementedShoppingCartServer) AddItem(context.Context, *Item) (*wrappers.StringValue, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AddItem not implemented")
}
func (UnimplementedShoppingCartServer) GetItem(context.Context, *wrappers.StringValue) (*Item, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetItem not implemented")
}
func (UnimplementedShoppingCartServer) mustEmbedUnimplementedShoppingCartServer() {}

// UnsafeShoppingCartServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to ShoppingCartServer will
// result in compilation errors.
type UnsafeShoppingCartServer interface {
	mustEmbedUnimplementedShoppingCartServer()
}

func RegisterShoppingCartServer(s *grpc.Server, srv ShoppingCartServer) {
	s.RegisterService(&_ShoppingCart_serviceDesc, srv)
}

func _ShoppingCart_AddItem_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Item)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ShoppingCartServer).AddItem(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/ecommerce.ShoppingCart/AddItem",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ShoppingCartServer).AddItem(ctx, req.(*Item))
	}
	return interceptor(ctx, in, info, handler)
}

func _ShoppingCart_GetItem_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(wrappers.StringValue)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ShoppingCartServer).GetItem(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/ecommerce.ShoppingCart/GetItem",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ShoppingCartServer).GetItem(ctx, req.(*wrappers.StringValue))
	}
	return interceptor(ctx, in, info, handler)
}

var _ShoppingCart_serviceDesc = grpc.ServiceDesc{
	ServiceName: "ecommerce.ShoppingCart",
	HandlerType: (*ShoppingCartServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "addItem",
			Handler:    _ShoppingCart_AddItem_Handler,
		},
		{
			MethodName: "getItem",
			Handler:    _ShoppingCart_GetItem_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "api/v1/shopping_cart.proto",
}
