# gRPC Gateway
gRPC Gateway plugin reads service definitions from protobuf and generates a reverse proxy server, which translates a restful JSON API into gRPC:

| ![grpc](../pics/grpc-gateway.png) |
|:--:|
| <b>gRPC Gateway</b>|

## testing
```
make
./bin/server
```
```
curl -X POST http://localhost:8081/v1/item -d '{"sku": "WC-JT-MD-PP", "description": "Winter Collection, Jackets, Medium, Purple", "price": 69.90, "quantity": 1}'; echo

curl http://localhost:8081/v1/item/WC-JT-MD-PP; echo
```