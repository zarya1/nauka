package main

import (
	"context"
	"errors"
	"log"
	"net"
        "net/http"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	wrapper "github.com/golang/protobuf/ptypes/wrappers"
	api "pirs/grpc-gateway/api/v1"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	hostname = "localhost"
	grpcPort = ":8400"
)

type server struct {
	api.UnimplementedShoppingCartServer
	itemMap map[string]*api.Item
}

func (s *server) AddItem(ctx context.Context, in *api.Item) (*wrapper.StringValue, error) {
	if s.itemMap == nil {
		s.itemMap = make(map[string]*api.Item)
	}
	s.itemMap[in.Sku] = in
	return &wrapper.StringValue{Value: in.Sku}, nil
}

func (s *server) GetItem(ctx context.Context, in *wrapper.StringValue) (*api.Item, error) {
	value, exists := s.itemMap[in.Value]
	if exists {
		return value, nil
	}
	return nil, errors.New("Item does not exist for the sku" + in.Value)
}

func main() {
	lis, err := net.Listen("tcp", grpcPort)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

        go func(){
            httpGateway()
	}()

	s := grpc.NewServer()
	api.RegisterShoppingCartServer(s, &server{})
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

func httpGateway() {
        ctx := context.Background()
        ctx, cancel := context.WithCancel(ctx)
        defer cancel()

        mux := runtime.NewServeMux()
        opts := []grpc.DialOption{grpc.WithInsecure()}
        err := api.RegisterShoppingCartHandlerFromEndpoint(ctx, mux, hostname+grpcPort, opts)
        if err != nil {
                log.Fatalf("Fail to register gRPC service endpoint: %v", err)
                return
        }
        if err := http.ListenAndServe(":8081", mux); err != nil {
                log.Fatalf("Could not setup HTTP endpoint: %v", err)
        }
}

