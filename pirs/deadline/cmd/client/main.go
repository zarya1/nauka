package main

import (
	"context"
        api "pirs/deadline/api/v1"
	"google.golang.org/grpc"
	"google.golang.org/grpc/status"
	"log"
	"time"
	"flag"
)

const (
	address = "localhost:50051"
)

func main() {
	var timeout int
	flag.IntVar(&timeout, "timeout", 2, "timeout")
        flag.Parse()

	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	client := api.NewOrderManagementClient(conn)

	clientDeadline := time.Now().Add(time.Duration(time.Duration(timeout) * time.Second))
	ctx, cancel := context.WithDeadline(context.Background(), clientDeadline)
	defer cancel()

	order1 := api.Order{Id: "256", Items:[]string{"WC-JT-MD-BK", "SE-RS-10-RD"}, Destination:"Sao Paulo, SP", Total:390.00}
	res, addErr := client.AddOrder(ctx, &order1)

	if addErr != nil {
		got := status.Code(addErr)
		log.Printf("Error Occured -> addOrder : , %v:", got)

	} else {
		log.Print("AddOrder Response -> ", res.Value)
	}
}
