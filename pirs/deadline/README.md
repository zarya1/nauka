# Deadline
### Problem statement
The microservice architecture involves a lot of network traffic and, inevitably, network problems sometimes occur, causing API calls to fail or take longer than expected. You need to design your services with the expectation of such incidents in mind. Timeouts are a fundamental concept in computer networking, often configurable with a parameter setting a time limit on network response; after the limit has passed, the operation is aborted rather than waiting indefinitely.
### Pitfalls to avoid
The effects of a bad timeout setting often don't become apparent until it's too late: it's peak time, your traffic has reached its highest point, and all your services have frozen at the same time. It is not good.

For instance, each service naively uses a default timeout of 1 second. While the Cart service receives a response from the Order service under the 1s timeout, everything will look fine:
| ![grpc](../pics/timeout2.png) |
|:--:|

But let's say that suddenly the Item service has a longer response time for the Order service, which means its response time for the Cart service will now be above 1s:

| ![grpc](../pics/timeout3.png) |
|:--:|

Item and order services are doing their work, not knowing that the cart service will drop the work because the total response time is overdue. However, this resource leak could soon be catastrophic: as the Cart-to-Order calls are expiring, the Cart will likely try again, causing the load on the Order to increase. This, in turn, causes the load on the Item to increase and eventually all services will stop responding.

### Setting a Timeout
When setting the timeout for an API call to another service, a good place to start would be the service level agreements (SLAs) for that service. Often, SLAs are based on latency percentiles, which is a value below which a certain percentage of latencies falls. For example, a system might have a 99th percentile latency of 350 ms; this would mean that 99% of latencies are below 350 ms. Let's say the request service endpoint has a 99th percentile latency of 500 ms. Setting the timeout for this call to 500ms would ensure that no call would take longer than 500ms, while returning errors for the rest and accepting an error rate of at most 1% (keeping its SLA). This is an example of how timeouts can be combined with latency information to provide predictable behavior.
### testing the example
```
make
```
start the server:
```
./bin/server
```
then, run the client:
```
./bin/client
```
run the client for few times. it will allow you to verify the timeout:
```
./bin/client 
2021/06/17 11:24:09 AddOrder Response -> Order Added: 256

./bin/client 
2021/06/17 11:24:12 Error Occured -> addOrder : , DeadlineExceeded:
```
### Error Budgets
#### Problem statement
The eternal tension between product development teams and SRE teams comes from the fact that they are measured on different metrics[^1]. The former is encouraged to promote new code as quickly as possible and the latter is encouraged to resist a high rate of change (while maintaining the reliability of the service).
There are also different opinions on the level of effort that should be put into engineering practices, such as the following:
- software fault tolerance
- testing
- push frequency
- canary duration and size
#### Relax tensions by working together
The two teams jointly define a quarterly error budget based on the services' SLO[^2], a clear and objective metric that determines how unreliable the service is allowed to be within a single quarter. This metric removes the politics from negotiations between the SRE and the product developers when deciding how much risk to allow. 
Recommended practices:
- Product Management defines an SLO, which sets an expectation of how much uptime the service should have per quarter.
- The actual uptime is measured by a neutral third party: the monitoring system.
- The difference between these two numbers is the "budget" of how much "unreliability" is remaining for the quarter.
- As long as the uptime measured is above the SLO, new releases can be pushed.
#### Example
Imagine that a service’s SLO is to successfully serve 99.999% of all queries per quarter[^1]. This means that the service’s error budget is a failure rate of 0.001% for a given quarter. If a problem causes us to fail 0.0002% of the expected queries for the quarter, the problem spends 20% of the service’s quarterly error budget.
#### Benefits
The main benefit of an error budget is that it provides a common incentive that allows both product development and SRE to focus on finding the right balance between innovation and reliability.

[^1]: [SRE Book, chapter 03](https://sre.google/sre-book/embracing-risk/).
[^2]: [SRE Book, chapter 04](https://sre.google/sre-book/service-level-objectives/).
