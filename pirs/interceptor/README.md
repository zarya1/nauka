# Interceptors
Interceptors are one of the main extension mechanisms in gRPC to meet requirements such as logging, authentication, authorization, metrics, tracing, or any other common logic before and after the execution of the remote function, for either client or server applications.:

| ![grpc](../pics/interceptor-ss.png) |
|:--:|


| ![grpc](../pics/interceptor-cs.png) |
|:--:|


## testing
```
make
```
```
./bin/server 
```
```
./bin/client
```
### server log output
```
2021/06/16 19:19:28 ======= [Server Interceptor]  /ecommerce.OrderManagement/AddOrder
2021/06/16 19:19:28  Pre Proc Message : id:"256"  items:"WC-JT-MD-BK"  items:"SE-RS-10-RD"  total:390  destination:"Sao Paulo, SP"
2021/06/16 19:19:28 Order :  256  -> Added
2021/06/16 19:19:28  Post Proc Message : value:"Order Added: 256"
2021/06/16 19:19:28 ====== [Server Stream Interceptor]  /ecommerce.OrderManagement/searchOrders
2021/06/16 19:19:28 ====== [Server Stream Interceptor Wrapper] Receive a message (Type: *wrapperspb.StringValue) at 2021-06-16T19:19:28-03:00
2021/06/16 19:19:28 Matching Order Found : 532 -> Writing Order to the stream ... 
2021/06/16 19:19:28 ====== [Server Stream Interceptor Wrapper] Send a message (Type: *v1.Order) at 2021-06-16T19:19:28-03:00
2021/06/16 19:19:28 Matching Order Found : 221 -> Writing Order to the stream ... 
2021/06/16 19:19:28 ====== [Server Stream Interceptor Wrapper] Send a message (Type: *v1.Order) at 2021-06-16T19:19:28-03:00
```
 ### client log output
```
2021/06/16 19:19:28 Method : /ecommerce.OrderManagement/addOrder
2021/06/16 19:19:28 value:"Order Added: 256"
2021/06/16 19:19:28 AddOrder Response -> Order Added: 256
2021/06/16 19:19:28 ======= [Client Interceptor]  /ecommerce.OrderManagement/searchOrders
2021/06/16 19:19:28 ====== [Client Stream Interceptor] Send a message (Type: *wrapperspb.StringValue) at 2021-06-16T19:19:28-03:00
2021/06/16 19:19:28 ====== [Client Stream Interceptor] Receive a message (Type: *v1.Order) at 2021-06-16T19:19:28-03:00
2021/06/16 19:19:28 Search Result : id:"532" items:"WC-JT-MD-PP" items:"SE-RS-10-OG" total:950 destination:"Sao Paulo, SP"
2021/06/16 19:19:28 ====== [Client Stream Interceptor] Receive a message (Type: *v1.Order) at 2021-06-16T19:19:28-03:00
2021/06/16 19:19:28 Search Result : id:"221" items:"WC-JT-MD-PP" items:"SE-RS-10-BL" total:580 destination:"Sao Paulo, SP"
2021/06/16 19:19:28 ====== [Client Stream Interceptor] Receive a message (Type: *v1.Order) at 2021-06-16T19:19:28-03:00
2021/06/16 19:19:28 EOF
```