package main

import (
	"context"
	epb "google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/resolver"
	"google.golang.org/grpc/status"
	"log"
	"fmt"
	api "pirs/client-lb/api/v1"
	wrapper "github.com/golang/protobuf/ptypes/wrappers"
	"time"
)

const (
	orderScheme      = "ordermanagement"
	orderServiceName = "lb.order.grpc.io"
)

var addrs = []string{"localhost:50051", "localhost:50052"}

func callGetOrder(client api.OrderManagementClient, orderID string) {

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	res, getOrderErr := client.GetOrder(ctx, &wrapper.StringValue{Value: orderID})

	if getOrderErr != nil {
		errorCode := status.Code(getOrderErr)
		if errorCode == codes.InvalidArgument {
			log.Printf("Invalid Argument Error : %s", errorCode)
			errorStatus := status.Convert(getOrderErr)
			for _, d := range errorStatus.Details() {
				switch info := d.(type) {
				case *epb.BadRequest_FieldViolation:
					log.Printf("Request Field Invalid: %s", info)
				default:
					log.Printf("Unexpected error type: %s", info)
				}
			}
		} else {
			log.Printf("Unhandled error : %s ", errorCode)
		}
	} else {
		log.Print("GetOrder Response -> ", res)
	}

}

func makeRPCs(cc *grpc.ClientConn, n int) {
	client := api.NewOrderManagementClient(cc)
	for i := 0; i < n; i++ {
		callGetOrder(client, "325")
	}
}

func main() {

	//the default LB algorithm is "pick firt"
	pickfirstConn, err := grpc.Dial(
		fmt.Sprintf("%s:///%s", orderScheme, orderServiceName),
		grpc.WithInsecure(),
	)
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer pickfirstConn.Close()

	log.Println("==== Calling OrderManagement.GetOrder with pick_first algorithm ====")
	makeRPCs(pickfirstConn, 10)

	// using round_robin LB algorithm
	roundrobinConn, err := grpc.Dial(
		fmt.Sprintf("%s:///%s", orderScheme, orderServiceName),
		grpc.WithBalancerName("round_robin"),
		grpc.WithInsecure(),
	)
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer roundrobinConn.Close()

	log.Println("==== Calling OrderManagement.GetOrder with round_robin algorithm ====")
	makeRPCs(roundrobinConn, 10)
}

//resolver implementation
type orderResolverBuilder struct{}

func (*orderResolverBuilder) Build(target resolver.Target, cc resolver.ClientConn, opts resolver.BuildOptions) (resolver.Resolver, error) {
	r := &orderResolver{
		target: target,
		cc:     cc,
		addrsStore: map[string][]string{
			orderServiceName: addrs,
		},
	}
	r.start()
	return r, nil
}
func (*orderResolverBuilder) Scheme() string { return orderScheme }

type orderResolver struct {
	target     resolver.Target
	cc         resolver.ClientConn
	addrsStore map[string][]string
}

func (r *orderResolver) start() {
	addrStrs := r.addrsStore[r.target.Endpoint]
	addrs := make([]resolver.Address, len(addrStrs))
	for i, s := range addrStrs {
		addrs[i] = resolver.Address{Addr: s}
	}
	r.cc.UpdateState(resolver.State{Addresses: addrs})
}
func (*orderResolver) ResolveNow(o resolver.ResolveNowOptions) {}
func (*orderResolver) Close(){}

func init() {
        resolver.Register(&orderResolverBuilder{})
}
