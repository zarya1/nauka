package main

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/wrappers"
	wrapper "github.com/golang/protobuf/ptypes/wrappers"
	api "pirs/client-lb/api/v1"
	epb "google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"io"
	"log"
	"net"
	"strings"
	"sync"
)

var (
	ports          = []string{":50051", ":50052"}
	orderBatchSize = 3
)

var orderMap = make(map[string]api.Order)

type server struct {
	api.UnimplementedOrderManagementServer
	orderMap map[string]*api.Order
	srv string
}

func (s *server) AddOrder(ctx context.Context, orderReq *api.Order) (*wrappers.StringValue, error) {

	if orderReq.Id == "-1" {
		log.Printf("Order ID is invalid! -> Received Order ID %s", orderReq.Id)

		errorStatus := status.New(codes.InvalidArgument, "Invalid information received")
		ds, err := errorStatus.WithDetails(
			&epb.BadRequest_FieldViolation{
				Field:"ID",
				Description: fmt.Sprintf("Order ID received is not valid %s : %s", orderReq.Id, orderReq.Description),
			},
		)
		if err != nil {
			return nil, errorStatus.Err()
		}

		return nil, ds.Err()
	} else {
		orderMap[orderReq.Id] = *orderReq
		log.Println("Order : ", orderReq.Id, " -> Added")
		return &wrapper.StringValue{Value: "Order Added: " + orderReq.Id}, nil
	}
}

func (s *server) GetOrder(ctx context.Context, orderId *wrapper.StringValue) (*api.Order, error) {
	log.Println("using server at: "+s.srv)
	ord := orderMap[orderId.Value]
	return &ord, nil
}

func (s *server) SearchOrders(searchQuery *wrappers.StringValue, stream api.OrderManagement_SearchOrdersServer) error {

	for key, order := range orderMap {
		for _, itemStr := range order.Items {
			if strings.Contains(itemStr, searchQuery.Value) {
				log.Print("Matching Order Found : "+key, " -> Writing Order to the stream ... ")
				stream.Send(&order)
				break
			}
		}
	}

	return nil
}

func (s *server) UpdateOrders(stream api.OrderManagement_UpdateOrdersServer) error {

	ordersStr := "Updated Order IDs : "
	for {
		order, err := stream.Recv()
		if err == io.EOF {
			return stream.SendAndClose(&wrapper.StringValue{Value: "Orders processed " + ordersStr})
		}
		orderMap[order.Id] = *order

		log.Printf("Order ID ", order.Id, ": Updated")
		ordersStr += order.Id + ", "
	}
}

func (s *server) ProcessOrders(stream api.OrderManagement_ProcessOrdersServer) error {

	batchMarker := 1
	var combinedShipmentMap = make(map[string]api.CombinedShipment)
	for {
		orderId, err := stream.Recv()
		log.Println("Reading Proc order ... ", orderId)
		if err == io.EOF {

			log.Println("EOF ", orderId)

			for _, comb := range combinedShipmentMap {
				stream.Send(&comb)
			}
			return nil
		}
		if err != nil {
			log.Println(err)
			return err
		}

		destination := orderMap[orderId.GetValue()].Destination
		shipment, found := combinedShipmentMap[destination]

		if found {
			ord := orderMap[orderId.GetValue()]
			shipment.OrdersList = append(shipment.OrdersList, &ord)
			combinedShipmentMap[destination] = shipment
		} else {
			comShip := api.CombinedShipment{Id: "cmb - " + (orderMap[orderId.GetValue()].Destination), Status: "Processed!",}
			ord := orderMap[orderId.GetValue()]
			comShip.OrdersList = append(shipment.OrdersList, &ord)
			combinedShipmentMap[destination] = comShip
			log.Print(len(comShip.OrdersList), comShip.GetId())
		}

		if batchMarker == orderBatchSize {
			for _, comb := range combinedShipmentMap {
				log.Print("Shipping : ", comb.Id, " -> ", len(comb.OrdersList))
				stream.Send(&comb)
			}
			batchMarker = 0
			combinedShipmentMap = make(map[string]api.CombinedShipment)
		} else {
			batchMarker++
		}
	}
}

func startServer(port string) {
	initSampleData()
        lis, err := net.Listen("tcp", port)
        if err != nil {
                log.Fatalf("failed to listen: %v", err)
        }
        s := grpc.NewServer()
	api.RegisterOrderManagementServer(s, &server{srv: port})
        log.Printf("serving on %s\n", port)
        if err := s.Serve(lis); err != nil {
                log.Fatalf("failed to serve: %v", err)
        }
}

func main() {
        var wg sync.WaitGroup
        for _, port := range ports {
                wg.Add(1)
                go func(port string) {
                        defer wg.Done()
                        startServer(port)
                }(port)
        }
        wg.Wait()
}

func initSampleData() {
        orderMap["221"] = api.Order{Id: "221", Items: []string{"WC-JT-MD-PP", "SE-RS-10-BL"}, Destination: "Sao Paulo, SP", Total: 580.00}
        orderMap["325"] = api.Order{Id: "325", Items: []string{"WC-JT-MD-WH"}, Destination: "Joao Pessoa, PB", Total: 340.00}
        orderMap["213"] = api.Order{Id: "213", Items: []string{"SE-RS-10-GR", "WC-JT-MD-WH"}, Destination: "Sao Paulo, SP", Total: 730.00}
        orderMap["428"] = api.Order{Id: "428", Items: []string{"SE-RS-10-BL"}, Destination: "Joao Pessoa, PB", Total: 620.00}
        orderMap["532"] = api.Order{Id: "532", Items: []string{"WC-JT-MD-PP", "SE-RS-10-OG"}, Destination: "Sao Paulo, SP", Total: 950.00}
}
