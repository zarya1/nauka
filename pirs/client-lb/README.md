# Client Load Balancing
While gRPC can use any HTTP/2 compliant server load balancer, it also allows for the implementation of client-level load balancing. In this approach, the client is aware of multiple backend gRPC servers and chooses one to use for calls. All load balancing logic is implemented as a client-side component. The client can then query it for the best gRPC server to connect to.
#### setting up a client-side LB
```
cmd/client/
└── main.go
```
```
 62         //the default LB algorithm is "pick firt"
 63         pickfirstConn, err := grpc.Dial(
 64                 fmt.Sprintf("%s:///%s", orderScheme, orderServiceName),
 65                 grpc.WithInsecure(),
 66         )
``` 
```
 75         // using round_robin LB algorithm
 76         roundrobinConn, err := grpc.Dial(
 77                 fmt.Sprintf("%s:///%s", orderScheme, orderServiceName),
 78                 grpc.WithBalancerName("round_robin"),
 79                 grpc.WithInsecure(),
 80         )
```
#### Testing the example
building
```
make
```
running the server
```
./bin/server
```
```
2021/07/12 15:13:04 serving on :50052
2021/07/12 15:13:04 serving on :50051
```
running the client
```
./bin/client
```
```
2021/07/12 15:16:32 ==== Calling OrderManagement.GetOrder with pick_first algorithm ====
2021/07/12 15:16:32 GetOrder Response -> id:"325"  items:"WC-JT-MD-WH"  total:340  destination:"Joao Pessoa, PB"
2021/07/12 15:16:32 GetOrder Response -> id:"325"  items:"WC-JT-MD-WH"  total:340  destination:"Joao Pessoa, PB"
2021/07/12 15:16:32 GetOrder Response -> id:"325"  items:"WC-JT-MD-WH"  total:340  destination:"Joao Pessoa, PB"
2021/07/12 15:16:32 GetOrder Response -> id:"325"  items:"WC-JT-MD-WH"  total:340  destination:"Joao Pessoa, PB"
2021/07/12 15:16:32 GetOrder Response -> id:"325"  items:"WC-JT-MD-WH"  total:340  destination:"Joao Pessoa, PB"
2021/07/12 15:16:32 GetOrder Response -> id:"325"  items:"WC-JT-MD-WH"  total:340  destination:"Joao Pessoa, PB"
2021/07/12 15:16:32 GetOrder Response -> id:"325"  items:"WC-JT-MD-WH"  total:340  destination:"Joao Pessoa, PB"
2021/07/12 15:16:32 GetOrder Response -> id:"325"  items:"WC-JT-MD-WH"  total:340  destination:"Joao Pessoa, PB"
2021/07/12 15:16:32 GetOrder Response -> id:"325"  items:"WC-JT-MD-WH"  total:340  destination:"Joao Pessoa, PB"
2021/07/12 15:16:32 GetOrder Response -> id:"325"  items:"WC-JT-MD-WH"  total:340  destination:"Joao Pessoa, PB"
2021/07/12 15:16:32 ==== Calling OrderManagement.GetOrder with round_robin algorithm ====
2021/07/12 15:16:32 GetOrder Response -> id:"325"  items:"WC-JT-MD-WH"  total:340  destination:"Joao Pessoa, PB"
2021/07/12 15:16:32 GetOrder Response -> id:"325"  items:"WC-JT-MD-WH"  total:340  destination:"Joao Pessoa, PB"
2021/07/12 15:16:32 GetOrder Response -> id:"325"  items:"WC-JT-MD-WH"  total:340  destination:"Joao Pessoa, PB"
2021/07/12 15:16:32 GetOrder Response -> id:"325"  items:"WC-JT-MD-WH"  total:340  destination:"Joao Pessoa, PB"
2021/07/12 15:16:32 GetOrder Response -> id:"325"  items:"WC-JT-MD-WH"  total:340  destination:"Joao Pessoa, PB"
2021/07/12 15:16:32 GetOrder Response -> id:"325"  items:"WC-JT-MD-WH"  total:340  destination:"Joao Pessoa, PB"
2021/07/12 15:16:32 GetOrder Response -> id:"325"  items:"WC-JT-MD-WH"  total:340  destination:"Joao Pessoa, PB"
2021/07/12 15:16:32 GetOrder Response -> id:"325"  items:"WC-JT-MD-WH"  total:340  destination:"Joao Pessoa, PB"
2021/07/12 15:16:32 GetOrder Response -> id:"325"  items:"WC-JT-MD-WH"  total:340  destination:"Joao Pessoa, PB"
2021/07/12 15:16:32 GetOrder Response -> id:"325"  items:"WC-JT-MD-WH"  total:340  destination:"Joao Pessoa, PB"
```
server output after client calls
```
2021/07/12 15:16:32 using server at: :50051
2021/07/12 15:16:32 using server at: :50051
2021/07/12 15:16:32 using server at: :50051
2021/07/12 15:16:32 using server at: :50051
2021/07/12 15:16:32 using server at: :50051
2021/07/12 15:16:32 using server at: :50051
2021/07/12 15:16:32 using server at: :50051
2021/07/12 15:16:32 using server at: :50051
2021/07/12 15:16:32 using server at: :50051
2021/07/12 15:16:32 using server at: :50051
2021/07/12 15:16:32 using server at: :50052
2021/07/12 15:16:32 using server at: :50051
2021/07/12 15:16:32 using server at: :50052
2021/07/12 15:16:32 using server at: :50051
2021/07/12 15:16:32 using server at: :50052
2021/07/12 15:16:32 using server at: :50051
2021/07/12 15:16:32 using server at: :50052
2021/07/12 15:16:32 using server at: :50051
2021/07/12 15:16:32 using server at: :50052
2021/07/12 15:16:32 using server at: :50051
```