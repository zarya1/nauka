# Retry
Continuing from where we left off in the [<b>Deadline</b>](../deadline/README.md) example (PLEASE, READ IT BEFORE!), let's learn how to use retries.

### 50th percentile and 99th percentile latency
| ![grpc](../pics/timeout2.png) |
|:--:|

Based on the latency of each request, the Cart service groups the requests into percentiles. You can view latency metrics for 50th percentile and 99th percentile latency:
- 50th percentile latency: The maximum latency, in seconds, for the fastest 50% of all requests. For example, if the 50th percentile latency is 0.5 seconds, then the Cart service processed 50% of requests in less than 0.5 seconds. This metric is sometimes called the median latency.
- 99th percentile latency: The maximum latency, in seconds, for the fastest 99% of requests. For example, if the 99th percentile latency is 2 seconds, then the Cart service processed 99% of requests in less than 2 seconds.
#### Setting retry options
Use the 99th and 50th percentile latency to configure retries:
```
cmd/client/
└── main.go
```
settings for overall calls...
```
 25         opts := []grpc_retry.CallOption{
 26                 grpc_retry.WithBackoff(grpc_retry.BackoffLinear(100 * time.Millisecond)),    <-- waits for a fixed period of time between calls
 27                 grpc_retry.WithMax(3),                                                       <-- max retries 
 28         }
 29         conn, err := grpc.Dial(address, grpc.WithInsecure(), 
 30                 grpc.WithStreamInterceptor(grpc_retry.StreamClientInterceptor(opts...)),
 31                 grpc.WithUnaryInterceptor(grpc_retry.UnaryClientInterceptor(opts...)),
 32         )
```
settings for specific call...
```
 41         ctx, cancel := context.WithTimeout(context.Background(), clientDeadline)             <-- the 99th percentile latency
 42 
 43         defer cancel()
 44 
 45         order1 := api.Order{Id: "256", Items:[]string{"WC-JT-MD-BK", "SE-RS-10-RD"}, Destination:"Sao Paulo, SP", Total:390.00}
 46 
 47         res, addErr := client.AddOrder(
 48                 ctx,
 49                 &order1,
 50                 grpc_retry.WithMax(3),                                                       <-- max retries 
 51                 grpc_retry.WithPerRetryTimeout(time.Duration(timeout_median)*time.Second),   <-- the 50th percentile latency (the median)
 52         )
 ```
### testing the example
```
make
```
start the server:
```
./bin/server
```
then, run the client:
```
./bin/client
```
run the client for few times. it will allow you to verify the retries and timeouts:
```
./bin/client 
2021/06/17 11:24:09 AddOrder Response -> Order Added: 256

./bin/client 
2021/06/17 11:24:12 Error Occured -> addOrder : , DeadlineExceeded:
```