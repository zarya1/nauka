package main

import (
	"context"
        api "pirs/retry/api/v1"
	"github.com/grpc-ecosystem/go-grpc-middleware/retry"
	"google.golang.org/grpc"
	"google.golang.org/grpc/status"
	"log"
	"time"
	"flag"
)

const (
	address = "localhost:50051"
)

func main() {
	var timeout int
	flag.IntVar(&timeout, "timeout", 2, "timeout")
	var timeout_median int
    flag.IntVar(&timeout_median, "timeout_median", 1, "timeout median")
	flag.Parse()

	opts := []grpc_retry.CallOption{
                grpc_retry.WithBackoff(grpc_retry.BackoffLinear(100 * time.Millisecond)),
                grpc_retry.WithMax(3),
        }
	conn, err := grpc.Dial(address, grpc.WithInsecure(), 
                grpc.WithStreamInterceptor(grpc_retry.StreamClientInterceptor(opts...)),
                grpc.WithUnaryInterceptor(grpc_retry.UnaryClientInterceptor(opts...)),
        )

	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	client := api.NewOrderManagementClient(conn)

    clientDeadline := time.Second*time.Duration(timeout)
	ctx, cancel := context.WithTimeout(context.Background(), clientDeadline)

	defer cancel()

	order1 := api.Order{Id: "256", Items:[]string{"WC-JT-MD-BK", "SE-RS-10-RD"}, Destination:"Sao Paulo, SP", Total:390.00}

	res, addErr := client.AddOrder(
		ctx,
		&order1,
		grpc_retry.WithMax(3),
		grpc_retry.WithPerRetryTimeout(time.Duration(timeout_median)*time.Second),
	)

	if addErr != nil {
		got := status.Code(addErr)
		log.Printf("Error Occured -> addOrder : , %v:", got)

	} else {
		log.Print("AddOrder Response -> ", res.Value)
	}
}
