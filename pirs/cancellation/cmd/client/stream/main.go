package main

import (
	"context"
	wrapper "github.com/golang/protobuf/ptypes/wrappers"
	api "pirs/cancellation/api/v1"
	"google.golang.org/grpc"
	"io"
	"log"
	"time"
)

const (
	address = "localhost:50051"
)

func main() {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	client := api.NewOrderManagementClient(conn)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	streamProcOrder, _ := client.ProcessOrders(ctx)
	_ = streamProcOrder.Send(&wrapper.StringValue{Value:"325"})
	_ = streamProcOrder.Send(&wrapper.StringValue{Value:"213"})
	_ = streamProcOrder.Send(&wrapper.StringValue{Value:"428"})

	channel := make(chan bool, 1)

	go processOrder(streamProcOrder, channel)
	time.Sleep(time.Millisecond * 1000)

	// Cancelling the RPC
	cancel()
	log.Printf("RPC Status : %s", ctx.Err())

	_ = streamProcOrder.Send(&wrapper.StringValue{Value:"221"})
	_ = streamProcOrder.CloseSend()

	<- channel
}


func processOrder (streamProcOrder api.OrderManagement_ProcessOrdersClient, c chan bool) {
	for {
		combinedShipment, errProcOrder := streamProcOrder.Recv()
		if errProcOrder != nil {
			log.Printf("Error Receiving messages %v", errProcOrder)
			break
		} else {
			if errProcOrder == io.EOF {
				break
			}
			log.Printf("Combined shipment : %s", combinedShipment.OrdersList)
		}
	}
	c <- true
}

