package main

import (
	"context"
        api "pirs/cancellation/api/v1"
	wrapper "github.com/golang/protobuf/ptypes/wrappers"
	"google.golang.org/grpc"
	"google.golang.org/grpc/status"
	"log"
)

const (
	address = "localhost:50051"
)

func main() {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	client := api.NewOrderManagementClient(conn)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

        var (
		res *api.Order
                getOrderErr error
	)

        signal := make(chan struct{})
	go func() {
		res, getOrderErr = client.GetOrder(ctx, &wrapper.StringValue{Value: "325"})
		signal <- struct{}{}
	}()
	go func() {
		res, getOrderErr = client.GetOrder(ctx, &wrapper.StringValue{Value: "325"})
		signal <- struct{}{}
	}()

        <-signal

	log.Println("Cancelling context... ")
	cancel()

	if getOrderErr != nil {
		got := status.Code(getOrderErr)
		log.Printf("Error Occured -> getOrder: , %v:", got)

	} else {
		log.Print("GetOrder Response -> ", res)
	}
}
