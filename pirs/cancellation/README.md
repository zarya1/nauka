# Cancellation
In a gRPc connection between a client and server application, the client and server make local and independent determinations of the success of the call.
The most common reason for canceling a request is because the client has encountered an error and no longer needs the response the server is processing. However, this technique can also be used in request hedging, where simultaneous duplicate requests are sent to the server to lessen the latency impact of an individual call. When the first response returns, the other requests are canceled because they are no longer needed.
### Examples
#### Canceling bidirectional streaming between the client and server services:
```
cmd/client/stream/
└── main.go
```
setting the cancel context...
```
 25         ctx, cancel := context.WithCancel(context.Background())
```
streaming the orders...
```
 28         streamProcOrder, _ := client.ProcessOrders(ctx)
 29         _ = streamProcOrder.Send(&wrapper.StringValue{Value:"325"})
 30         _ = streamProcOrder.Send(&wrapper.StringValue{Value:"213"})
 31         _ = streamProcOrder.Send(&wrapper.StringValue{Value:"428"}) 
```
Cancelling the streaming...
```
 38         // Cancelling the RPC
 39         cancel()
```
how to test it...
```
make
```
start the server:
```
./bin/server
```
then, run the client streaming:
```
./bin/client-stream 
2021/06/17 16:59:31 Combined shipment : [id:"325" items:"WC-JT-MD-WH" total:340 destination:"Joao Pessoa, PB" id:"428" items:"SE-RS-10-BL" total:620 destination:"Joao Pessoa, PB"]
2021/06/17 16:59:31 Combined shipment : [id:"213" items:"SE-RS-10-GR" items:"WC-JT-MD-WH" total:730 destination:"Sao Paulo, SP"]
2021/06/17 16:59:32 RPC Status : context canceled
2021/06/17 16:59:32 Error Receiving messages rpc error: code = Canceled desc = context canceled
```
server output...
```
2021/06/17 16:59:31 Reading Proc order ...  value:"325"
2021/06/17 16:59:31 1cmb - Joao Pessoa, PB
2021/06/17 16:59:31 Reading Proc order ...  value:"213"
2021/06/17 16:59:31 1cmb - Sao Paulo, SP
2021/06/17 16:59:31 Reading Proc order ...  value:"428"
2021/06/17 16:59:31 Shipping : cmb - Joao Pessoa, PB -> 2
2021/06/17 16:59:31 Shipping : cmb - Sao Paulo, SP -> 1
2021/06/17 16:59:32 Reading Proc order ...  value:"221"
2021/06/17 16:59:32 1cmb - Sao Paulo, SP
2021/06/17 16:59:32  Context Cacelled for this stream: -> context canceled
2021/06/17 16:59:32 Stopped processing any more order of this stream!
```
#### Canceling a duplicate request (when using request hedging)
```
cmd/client/fastest/
└── main.go
```
setting the cancel context...
```
 24         ctx, cancel := context.WithCancel(context.Background())
```
using concurrency to duplicate requests and canceling the slower response...
```
 32         signal := make(chan struct{})
 33         go func() {
 34                 res, getOrderErr = client.GetOrder(ctx, &wrapper.StringValue{Value: "325"})
 35                 signal <- struct{}{}
 36         }()
 37         go func() {
 38                 res, getOrderErr = client.GetOrder(ctx, &wrapper.StringValue{Value: "325"})
 39                 signal <- struct{}{}
 40         }()
 41 
 42         <-signal
 43 
 44         log.Println("Cancelling context... ")
 45         cancel()
```
how to test it...
```
make
```
start the server:
```
./bin/server
```
then, run the client request hedging:
```
 ./bin/client-fastest 
2021/06/17 17:13:21 Cancelling context... 
2021/06/17 17:13:21 GetOrder Response -> id:"325"  items:"WC-JT-MD-WH"  total:340  destination:"Joao Pessoa, PB"
```
server output:
```
2021/06/17 17:13:20 GetOrder invoked remotely
2021/06/17 17:13:20 GetOrder invoked remotely
2021/06/17 17:13:21 processed
2021/06/17 17:13:21 canceled
```