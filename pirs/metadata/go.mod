module pirs/metadata

go 1.16

require (
	github.com/golang/protobuf v1.5.2
	google.golang.org/genproto v0.0.0-20210712171009-5404628d0f46
	google.golang.org/grpc v1.39.0
	google.golang.org/protobuf v1.27.1
)
