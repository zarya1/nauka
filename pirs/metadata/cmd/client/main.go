package main

import (
	"context"
	api "pirs/metadata/api/v1"
	epb "google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"log"
	"time"
	"fmt"
	"google.golang.org/grpc/metadata"
)

const (
	address = "localhost:50051"
)

func main() {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	client := api.NewOrderManagementClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second * 5)
	defer cancel()

        // ****** Metadata : Creation *****
        md := metadata.Pairs(
                "timestamp", time.Now().Format(time.StampNano),
        )
        mdCtx := metadata.NewOutgoingContext(ctx, md)

        // RPC using the context with new metadata.
	var header, trailer metadata.MD

	//placing an order
	order1 := api.Order{Id: "101", Items:[]string{"WC-JT-MD-BK", "SE-RS-10-RD"}, Destination:"Sao Paulo, SP", Total:390.00}
	res, addOrderError := client.AddOrder(mdCtx, &order1, grpc.Header(&header), grpc.Trailer(&trailer))

	if addOrderError != nil {
		errorCode := status.Code(addOrderError)
		if errorCode == codes.InvalidArgument {
			log.Printf("Invalid Argument Error : %s", errorCode)
			errorStatus := status.Convert(addOrderError)
			for _, d := range errorStatus.Details() {
				switch info := d.(type) {
				case *epb.BadRequest_FieldViolation:
					log.Printf("Request Field Invalid: %s", info)
				default:
					log.Printf("Unexpected error type: %s", info)
				}
			}
		} else {
			log.Printf("Unhandled error : %s ", errorCode)
		}
	} else {
		log.Print("AddOrder Response -> ", res.Value)
		// Reading the headers
                if t, ok := header["timestamp"]; ok {
                   log.Printf("timestamp from header:\n")
                   for i, e := range t {
                        fmt.Printf(" %d. %s\n", i, e)
                   }
                } else {
                   log.Fatal("timestamp expected but doesn't exist in header")
                }
                if l, ok := header["location"]; ok {
                   log.Printf("location from header:\n")
                   for i, e := range l {
                        fmt.Printf(" %d. %s\n", i, e)
                   }
                } else {
                   log.Fatal("location expected but doesn't exist in header")
                }
	}

}
