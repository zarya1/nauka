# Metadata
When you need to share information about RPC calls that are not related to the RPC business context (e.g., security headers), you can use gRPC metadata (for sending or receiving).
#### writing metadata
```
cmd/client/
└── main.go
```
```
 31         // ****** Metadata : Creation *****
 32         md := metadata.Pairs(
 33                 "timestamp", time.Now().Format(time.StampNano),
 34         )
 35         mdCtx := metadata.NewOutgoingContext(ctx, md)
 36 
 37         // RPC using the context with new metadata.
 38         var header, trailer metadata.MD
 39 
 40         //placing an order
 41         order1 := api.Order{Id: "101", Items:[]string{"WC-JT-MD-BK", "SE-RS-10-RD"}, Destination:"Sao Paulo, SP",     Total:390.00}
 42         res, addOrderError := client.AddOrder(mdCtx, &order1, grpc.Header(&header), grpc.Trailer(&trailer))
```
```
cmd/server/
└── main.go
```
```
 67                 // Creating and sending a header.
 68                 header := metadata.New(map[string]string{"location": "Rio de Janeiro", "timestamp": time.Now().Format(time.StampNano)})
 69                 grpc.SendHeader(ctx, header)
```
#### reading metadata
```
cmd/client/
└── main.go
```
```
 65                 // Reading the headers
 66                 if t, ok := header["timestamp"]; ok {
 67                    log.Printf("timestamp from header:\n")
 68                    for i, e := range t {
 69                         fmt.Printf(" %d. %s\n", i, e)
 70                    }
 71                 } else {
 72                    log.Fatal("timestamp expected but doesn't exist in header")
 73                 }
 74                 if l, ok := header["location"]; ok {
 75                    log.Printf("location from header:\n")
 76                    for i, e := range l {
 77                         fmt.Printf(" %d. %s\n", i, e)
 78                    }
 79                 } else {
 80                    log.Fatal("location expected but doesn't exist in header")
 81                 }
```
```
cmd/server/
└── main.go
```
```
 55                 // ***** Reading Metadata from Client *****
 56                 md, metadataAvailable := metadata.FromIncomingContext(ctx)
 57                 if !metadataAvailable {
 58                         return nil, status.Errorf(codes.DataLoss, "Failed to get metadata")
 59                 }
 60                 if t, ok := md["timestamp"]; ok {
 61                         fmt.Printf("timestamp from metadata:\n")
 62                         for i, e := range t {
 63                                 fmt.Printf("====> Metadata %d. %s\n", i, e)
 64                         }
 65                 }
```
#### Testing the example
building
```
make
```
running
```
./bin/server
```
```
./bin/client
```
client output
```
2021/07/12 17:43:26 AddOrder Response -> Order Added: 101
2021/07/12 17:43:26 timestamp from header:
 0. Jul 12 17:43:26.262107408
2021/07/12 17:43:26 location from header:
 0. Rio de Janeiro
```
server output
```
2021/07/12 17:43:26 Order :  101  -> Added
timestamp from metadata:
====> Metadata 0. Jul 12 17:43:26.257424480
```