# gRPC sample code, written in GO
## Table of Contents
- [gRPC Gateway](./grpc-gateway/README.md)
- [Interceptors](./interceptor/README.md)
- [Deadline](./deadline/README.md)
- [Retry](./retry/README.md)
- [Cancellation](./cancellation/README.md)
- [Error treatment](./error-treatment/README.md)
- [Client side load balancing](./client-lb/README.md)
- [Metadata](./metadata/README.md)
---
| ![grpc](./pics/grpc.png) |
|:--:|
| <b>gRPC, in a nutshell</b>|
