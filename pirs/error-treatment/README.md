# Error treatment
gRPC client calls receives a response with a successful status or an error with the corresponding error status. The client application needs to be written in such a way that you handle all the potential errors and error conditions. The server application requires you to handle errors as well as generate the appropriate errors with the corresponding status code. The complete list of error codes can be found in [Status codes and their use in gRPC](https://github.com/grpc/grpc/blob/master/doc/statuscodes.md).
In this example, we are showing how error handling can be created using gRPC.
#### Testing the example
the client app...
```
cmd/client/
└── main.go
```
introducing a error...
```
29         order1 := api.Order{Id: "-1", Items:[]string{"WC-JT-MD-BK", "SE-RS-10-RD"}, Destination:"Sao Paulo, SP", Total:390.00}
```
treating a error...
```
 33         if addOrderError != nil {
 34                 errorCode := status.Code(addOrderError)
 35                 if errorCode == codes.InvalidArgument {
 36                         log.Printf("Invalid Argument Error : %s", errorCode)
 37                         errorStatus := status.Convert(addOrderError)
 38                         for _, d := range errorStatus.Details() {
 39                                 switch info := d.(type) {
 40                                 case *epb.BadRequest_FieldViolation:
 41                                         log.Printf("Request Field Invalid: %s", info)
 42                                 default:
 43                                         log.Printf("Unexpected error type: %s", info)
 44                                 }
 45                         }
 46                 } else {
 47                         log.Printf("Unhandled error : %s ", errorCode)
 48                 }
 49         } else {
 50                 log.Print("AddOrder Response -> ", res.Value)
 51         }
```
the server app...
```
cmd/server/
└── main.go
```
treating a error...
```
 34         if orderReq.Id == "-1" {
 35                 log.Printf("Order ID is invalid! -> Received Order ID %s", orderReq.Id)
 36 
 37                 errorStatus := status.New(codes.InvalidArgument, "Invalid information received")
 38                 ds, err := errorStatus.WithDetails(
 39                         &epb.BadRequest_FieldViolation{
 40                                 Field:"ID",
 41                                 Description: fmt.Sprintf("Order ID received is not valid %s : %s", orderReq.Id, orderReq.Description),
 42                         },
 43                 )
 44                 if err != nil {
 45                         return nil, errorStatus.Err()
 46                 }
 47 
 48                 return nil, ds.Err()
 49         } else {
 50                 orderMap[orderReq.Id] = *orderReq
 51                 log.Println("Order : ", orderReq.Id, " -> Added")
 52                 return &wrapper.StringValue{Value: "Order Added: " + orderReq.Id}, nil
 53         }
```
building and running...
```
make
```
```
./bin/server
```
```
./bin/client
```
server output
```
2021/06/17 17:51:32 Order ID is invalid! -> Received Order ID -1
```
client output
```
2021/06/17 17:51:32 Invalid Argument Error : InvalidArgument
2021/06/17 17:51:32 Request Field Invalid: field:"ID"  description:"Order ID received is not valid -1 : "
```