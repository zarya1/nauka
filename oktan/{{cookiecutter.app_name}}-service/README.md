# {{cookiecutter.app_name}} grpc server 
## building a docker image
```
TAG=0.0.1 IMAGE_NAME={{cookiecutter.image_registry_project_id}}/{{cookiecutter.app_name}} make build-docker
```
## deploy on k8s
```
kubectl apply -f deploy/k8s-manifests.yaml
```
## buiding grpc stub & skeleton
```
make build-pb
```
## buiding the grpc server 
```
make
```
## metrics & tracing
```
http://localhost:8082/debug/rpcz
http://localhost:8082/debug/tracez
```
