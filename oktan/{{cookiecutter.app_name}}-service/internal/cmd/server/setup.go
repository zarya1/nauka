package server

import (
	"fmt"
	"net"
	"sync"

	"go.uber.org/zap"
	"google.golang.org/grpc"

	"zarya1/oktan/internal/domain"
	grpc_protocol "zarya1/oktan/internal/protocol/grpc"
	rest_protocol "zarya1/oktan/internal/protocol/rest"
)

type CmdServer struct {
	Config

	service       *domain.Service
	server     *grpc.Server

	shutdown     bool
	shutdowns    chan struct{}
	shutdownLock sync.Mutex
}


type Config struct {
	Datacfg         string
	Hostname        string
	RPCPort         int
	HTTPPort        int
	DebugPort       int
}

func (c Config) RPCAddr() (string, error) {
	return fmt.Sprintf(":%d", c.RPCPort), nil
}

func (c Config) HTTPAddr() (string, error) {
        return fmt.Sprintf(":%d", c.HTTPPort), nil
}

func (c Config) DebugAddr() (string, error) {
        return fmt.Sprintf(":%d", c.DebugPort), nil
}

func New(config Config) (*CmdServer, error) {
	a := &CmdServer{
		Config:    config,
		shutdowns: make(chan struct{}),
	}
	setup := []func() error{
		a.setupLogger,
		a.setupService,
		a.setupServer,
	}
	for _, fn := range setup {
		if err := fn(); err != nil {
			return nil, err
		}
	}
	return a, nil
}

func (a *CmdServer) setupLogger() error {
	logger, err := zap.NewDevelopment()
	if err != nil {
		return err
	}
	zap.ReplaceGlobals(logger)
	return nil
}


func (a *CmdServer) setupService() error {
	var err error
	a.service, err = domain.NewService(
		a.Config.Datacfg,
	)
	return err
}


func (a *CmdServer) setupServer() error {
	serverConfig := &grpc_protocol.Config{
		{{cookiecutter.app_name_camelcase}}:  a.service,
	}

        var opts []grpc.ServerOption
        var err error

	debugAddr, err := a.DebugAddr()
        if err != nil {
                return err
        }

	a.server, err = grpc_protocol.NewGRPCServer(debugAddr, serverConfig, opts...)
	if err != nil {
		return err
	}

	rpcAddr, err := a.RPCAddr()
	if err != nil {
		return err
	}
	ln, err := net.Listen("tcp", rpcAddr)
	if err != nil {
		return err
	}

	httpAddr, err := a.HTTPAddr()
        if err != nil {
                return err
        }

        go func(){
		rest_protocol.HttpGateway(a.Config.Hostname, rpcAddr, httpAddr)
        }()

	go func() {
		if err := a.server.Serve(ln); err != nil {
			_ = a.Shutdown()
		}
	}()
	return err
}

func (a *CmdServer) Shutdown() error {
	a.shutdownLock.Lock()
	defer a.shutdownLock.Unlock()
	if a.shutdown {
		return nil
	}
	a.shutdown = true
	close(a.shutdowns)

	shutdown := []func() error{
		func() error {
			a.server.GracefulStop()
			return nil
		},
	}
	for _, fn := range shutdown {
		if err := fn(); err != nil {
			return err
		}
	}
	return nil
}

