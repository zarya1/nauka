package rest

import (
	"context"
	"log"
        "net/http"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	api "zarya1/oktan/api/v1"
	"google.golang.org/grpc"
)

func HttpGateway(hostname string, grpcport string, httpport string) {
        ctx := context.Background()
        ctx, cancel := context.WithCancel(ctx)
        defer cancel()

        mux := runtime.NewServeMux()
        opts := []grpc.DialOption{grpc.WithInsecure()}
        err := api.Register{{cookiecutter.app_name_camelcase}}HandlerFromEndpoint(ctx, mux, hostname+grpcport, opts)
        if err != nil {
                log.Fatalf("Fail to register gRPC service endpoint: %v", err)
                return
        }
        if err := http.ListenAndServe(httpport, mux); err != nil {
                log.Fatalf("Could not setup HTTP endpoint: %v", err)
        }
}
