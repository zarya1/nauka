package grpc

import (
	"context"
	"time"
        "net/http"
        "log"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
        api "zarya1/oktan/api/v1"

	grpc_zap "github.com/grpc-ecosystem/go-grpc-middleware/logging/zap"
	grpc_ctxtags "github.com/grpc-ecosystem/go-grpc-middleware/tags"
        wrapper "github.com/golang/protobuf/ptypes/wrappers"

	"go.opencensus.io/plugin/ocgrpc"
	"go.opencensus.io/stats/view"
        "go.opencensus.io/zpages"
        "go.opencensus.io/examples/exporter"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"


	"google.golang.org/grpc"
        "google.golang.org/grpc/health"

	healthpb "google.golang.org/grpc/health/grpc_health_v1"
)


type Config struct {
	{{cookiecutter.app_name_camelcase}} {{cookiecutter.app_name_camelcase}}
}

var _ api.{{cookiecutter.app_name_camelcase}}Server = (*grpcServer)(nil)

type grpcServer struct {
	api.Unimplemented{{cookiecutter.app_name_camelcase}}Server
	*Config
}

func newgrpcServer(config *Config) (*grpcServer, error) {
	srv := &grpcServer{
		Config: config,
	}
	return srv, nil
}

func NewGRPCServer(debugAddr string, config *Config, grpcOpts ...grpc.ServerOption) (
	*grpc.Server,
	error,
) {
	logger := zap.L().Named("server")
	zapOpts := []grpc_zap.Option{
		grpc_zap.WithDurationField(
			func(duration time.Duration) zapcore.Field {
				return zap.Int64(
					"grpc.time_ns",
					duration.Nanoseconds(),
				)
			},
		),
	}

        go func() {
                mux := http.NewServeMux()
                zpages.Handle(mux, "/debug")
                log.Fatal(http.ListenAndServe(debugAddr, mux))
        }()

        view.RegisterExporter(&exporter.PrintExporter{})

        if err := view.Register(ocgrpc.DefaultServerViews...); err != nil {
                log.Fatal(err)
        }

	grpcOpts = append(grpcOpts,
		grpc.StreamInterceptor(
			grpc_middleware.ChainStreamServer(
				grpc_ctxtags.StreamServerInterceptor(),
				grpc_zap.StreamServerInterceptor(logger, zapOpts...),
			)), grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			grpc_ctxtags.UnaryServerInterceptor(),
			grpc_zap.UnaryServerInterceptor(logger, zapOpts...),
		)),
		grpc.StatsHandler(&ocgrpc.ServerHandler{}),
	)
	gsrv := grpc.NewServer(grpcOpts...)

	hsrv := health.NewServer()
        hsrv.SetServingStatus("", healthpb.HealthCheckResponse_SERVING)
        healthpb.RegisterHealthServer(gsrv, hsrv)

	srv, err := newgrpcServer(config)
	if err != nil {
		return nil, err
	}
	api.Register{{cookiecutter.app_name_camelcase}}Server(gsrv, srv)
	return gsrv, nil
}

func (s *grpcServer) AddItem(ctx context.Context, item *api.Item) (*wrapper.StringValue, error) {

	id, err := s.{{cookiecutter.app_name_camelcase}}.AddItem(item)
        if err != nil {
                return nil, err
        }
        return id, nil
}

func (s *grpcServer) GetItem(ctx context.Context, id *wrapper.StringValue) (*api.Item, error) {

	item, err := s.{{cookiecutter.app_name_camelcase}}.GetItem(id)
        if err != nil {
                return nil, err
        }
        return item, nil
}

type {{cookiecutter.app_name_camelcase}} interface {
	AddItem(item *api.Item) (*wrapper.StringValue, error)
        GetItem(id *wrapper.StringValue) (*api.Item, error)
}
