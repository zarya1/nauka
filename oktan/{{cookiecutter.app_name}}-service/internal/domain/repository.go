package domain

import (
    api "zarya1/oktan/api/v1"
    wrapper "github.com/golang/protobuf/ptypes/wrappers"
)

type Repository interface {
    AddItem(item *api.Item) (*wrapper.StringValue, error)
    GetItem(id *wrapper.StringValue) (*api.Item, error)
}

func GetRepository() (Repository) {
    return NewRepo()
}
