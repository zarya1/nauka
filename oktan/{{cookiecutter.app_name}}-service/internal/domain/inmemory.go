// +build inmem

package domain

import (
	"errors"
	api "zarya1/oktan/api/v1"
	wrapper "github.com/golang/protobuf/ptypes/wrappers"
)

type Repo struct {
	m map[string]*api.Item
}

func NewRepo() Repository {
	var m = map[string]*api.Item{}
	return &Repo{
		m: m,
	}
}

func (r *Repo) AddItem(item *api.Item) (*wrapper.StringValue, error) {
        if r.m == nil {
                r.m = make(map[string]*api.Item)
        }
        r.m[item.Sku] = item
        return &wrapper.StringValue{Value: item.Sku}, nil
}

func (r *Repo) GetItem(id *wrapper.StringValue) (*api.Item, error) {
        value, exists := r.m[id.Value]
        if exists {
                return value, nil
        }
        return nil, errors.New("Item does not exist for the sku" + id.Value)
}
