package domain

import (
	api "zarya1/oktan/api/v1"
	wrapper "github.com/golang/protobuf/ptypes/wrappers"
)

type Service struct {
        repo Repository
}

func NewService(datacfg string) (*Service, error) {
        repo := GetRepository()

	service := &Service{
                repo: repo,
	}

	return service, nil
}

func (c *Service) AddItem(item *api.Item) (*wrapper.StringValue, error) {

        return c.repo.AddItem(item)
}

func (c *Service) GetItem(id *wrapper.StringValue) (*api.Item, error) {

        return c.repo.GetItem(id)
}
