package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	cmdsrv "zarya1/oktan/internal/cmd/server"
)

var ServiceImplementing = "inmemory"
var GitCommit = "undefined"

func main() {
	fmt.Println("Service implementing: " + ServiceImplementing)
        fmt.Println("Git commit: " + GitCommit)

	cli := &cli{}

	cmd := &cobra.Command{
		Use:     "oktan",
		PreRunE: cli.setupConfig,
		RunE:    cli.run,
	}

	if err := setupFlags(cmd); err != nil {
		log.Fatal(err)
	}

	if err := cmd.Execute(); err != nil {
		log.Fatal(err)
	}
}

type cli struct {
	cfg cfg
}

type cfg struct {
	cmdsrv.Config
}

func setupFlags(cmd *cobra.Command) error {

	cmd.Flags().String("config-file", "", "Path to config file.")

	datacfg := "inmem"
	cmd.Flags().String("data-cfg",
		datacfg,
		"data cfg string.")

	cmd.Flags().String("hostname",
                "localhost",
                "server hostname string.")

	cmd.Flags().Int("rpc-port",
		8400,
		"Port for RPC clients connections.")

	cmd.Flags().Int("http-port",
                8081,
                "Port for RPC http proxy connections.")

        cmd.Flags().Int("debug-port",
                8082,
                "Port for debug connections.")

	return viper.BindPFlags(cmd.Flags())
}


func (c *cli) setupConfig(cmd *cobra.Command, args []string) error {
	var err error

	configFile, err := cmd.Flags().GetString("config-file")
	if err != nil {
		return err
	}
	viper.SetConfigFile(configFile)

	if err = viper.ReadInConfig(); err != nil {
		// it's ok if config file doesn't exist
		if _, ok := err.(viper.ConfigFileNotFoundError); !ok {
			return err
		}
	}

	c.cfg.Datacfg = viper.GetString("data-cfg")
	c.cfg.Hostname = viper.GetString("hostname")
	c.cfg.RPCPort = viper.GetInt("rpc-port")
        c.cfg.HTTPPort = viper.GetInt("http-port")
        c.cfg.DebugPort = viper.GetInt("debug-port")

	return nil
}


func (c *cli) run(cmd *cobra.Command, args []string) error {
        var err error
        cmdServer, err := cmdsrv.New(c.cfg.Config)
        if err != nil {
                return err
        }
        sigc := make(chan os.Signal, 1)
        signal.Notify(sigc, syscall.SIGINT, syscall.SIGTERM)
        <-sigc
        return cmdServer.Shutdown()
}

