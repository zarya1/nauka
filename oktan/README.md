# gRPC Service Template

cookiecutter project for gRPC Service

### Prerequisites
- [go](https://golang.org/doc/install)
- [cookiecutter](https://cookiecutter.readthedocs.io/en/1.7.2/installation.html)

### Using cookiecutter to create a new gRPC Service project
```
cd ..
cookiecutter oktan/
```
### high level architecture design
![grpc service template](./grpc-template.png)
