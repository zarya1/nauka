# A Dummy Shopping Cart (gRPC project sample, written in GO)
## Table of Contents
- [Prerequisite](./prerequisite.md)
- [Project structure](./structure.md)
- [Building, Testing and Delivering ](./build.md)
- [Kind (kubernetes in Docker, for local test)](./kind.md)
- [Deploy with Helm](./helm.md)
- [gRPC features used in this project](./grpc_features.md)
- [Liveness, Readiness and Startup probes for Containers](./probes.md)
- [gRPC benchmarking and load testing tool](./load-testing.md)
---
| ![grpc](./pics/grpc.png) |
|:--:|
| <b>gRPC, in a nutshell</b>|