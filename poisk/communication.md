## Communication patterns
### Unary: a simple request/response
![grpc unary](./pics/grpcunary.png)
### Streaming: a sequence of requests/responses
![grpc stream](./pics/grpcstream.png)
