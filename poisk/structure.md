## Project structure
**IDL (protobuf) and generated code (client stub and server skeleton)**
```
api/
└── v1
    ├── shopping_cart_grpc.pb.go  <-- (generated) contains the client stub and the server skeleton
    ├── shopping_cart.pb.go       <-- (generated) contains the protocol buffer code to populate, serialize, and retrieve request and response message types
    └── shopping_cart.proto       <-- protocol buffer (IDL)
```
---
**Command-line interfaces (server and client applications)**
```
cmd/
├── client
│   └── main.go                   <-- command-line interface gRPC client sample 
└── server
    └── main.go                   <-- command-line interface gRPC server sample
```
---
**Application Server implementation**
```
internal/
├── auth
│   └── authorizer.go             <-- wrapper for security authorization 
├── cart
│   ├── cart.go                   <-- business logic implementation
│   ├── cart_inmem.go             <-- infrastructure layer logic implementation
│   ├── cart_repo.go              <-- infrastructure layer logic abstraction
│   ├── cart_test.go
│   └── error.go                  <-- wrapper for errors treatment
├── cmd
│   └── server
│       ├── cartserver.go         <-- wrapper for the server command-line interface
│       └── cartserver_test.go
├── config
│   ├── files.go                  <-- wrapper for auth tests
│   └── tls.go                    <-- wrapper for auth setup
└── protocol
    └── grpc
        ├── server.go             <-- gRPC server protocol implementation 
        └── server_test.go
```