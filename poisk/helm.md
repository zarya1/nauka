## Deploy with Helm
**Once you have your local images loaded into the node cluster, deploy it on Kubernetes (Kind)** 
```
deploy/
├── shoppingcart
│   ├── charts
│   ├── Chart.yaml
│   ├── templates
│   │   ├── deployment
│   │   │   └── cart-server.yaml
│   │   ├── namespace
│   │   │   └── shoppingcart.yaml
│   │   └── service
│   │       └── cart-server.yaml
│   └── values.yaml
└── shoppingcart-client
    ├── Chart.yaml
    ├── templates
    │   ├── deployment
    │   │   └── cart-client.yaml
    │   └── namespace
    │       └── shoppingcart-client.yaml
    └── values.yaml
```
### install the app server helm chart
```
helm install shoppingcart deploy/shoppingcart
```
### verify the app server running pod
```
kubectl get po,svc -n shoppingcart
NAME                              READY   STATUS    RESTARTS   AGE
pod/cart-server-cc98874d9-75cqw   1/1     Running   1          5h2m

NAME                  TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)    AGE
service/cart-server   ClusterIP   10.96.0.9    <none>        8400/TCP   5h2m
```
### install the app client helm chart
```
helm install shoppingcart-client deploy/shoppingcart-client
```
### verify the app client running pod
```
kubectl get po -n shoppingcart-client
NAME                          READY   STATUS    RESTARTS   AGE
cart-client-66f7cf8cc-2srgw   1/1     Running   0          105m
```



