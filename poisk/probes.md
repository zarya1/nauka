## Liveness, Readiness and Startup probes for Containers
Kubernetes uses probes[^1] to know the state of a container and find out if it is up and ready to receive requests. The gRPC ecosystem offers a grpc_health_probe[^2] command for this purpose. gRPC has a feature known as multiplexing, which runs multiple gRPC services on the same gRPC server, as well as using the same gRPC client connection for multiple gRPC client stubs. We are taking advantage of this feature to run the grpc_health_probe probe, as shown in the snippet below:
```
internal/
.
.
.
└── protocol
    └── grpc
        ├── server.go
```
```
94   gsrv := grpc.NewServer(grpcOpts...)                                 <-- create a new gRPC server
95
96   hsrv := health.NewServer()
97   hsrv.SetServingStatus("", healthpb.HealthCheckResponse_SERVING)
98   healthpb.RegisterHealthServer(gsrv, hsrv)                           <-- register the health probe service
99
100  srv, err := newgrpcServer(config)
101  if err != nil {
102      return nil, err
103  }
104  api.RegisterShoppingCartServer(gsrv, srv)                           <-- register the shopping cart service
```
The snippet below is showing how to use the grpc_health_probe on the Kubernetes deployment manifest:
```
deploy/shoppingcart/templates/deployment/
└── cart-server.yaml
```
```
32        # probes...
33        readinessProbe:
34          exec:
35            command: ["/app/grpc-health-probe", "-addr=:{{ .Values.rpcPort }}", "-tls=true", "-tls-ca-cert=/certs/ca.pem", "-tls-client-cert=/certs/client.pem", "-tls-client-key=/certs/client-key.pem", "-tls-server-name=cart-server.shoppingcart"]
36          initialDelaySeconds: 10
37        livenessProbe:
38          exec:
39            command: ["/app/grpc-health-probe", "-addr=:{{ .Values.rpcPort }}", "-tls=true", "-tls-ca-cert=/certs/ca.pem", "-tls-client-cert=/certs/client.pem", "-tls-client-key=/certs/client-key.pem", "-tls-server-name=cart-server.shoppingcart"]
40          initialDelaySeconds: 10  
```
 [^1]: [Configure Liveness, Readiness and Startup Probes](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/).
 [^2]: [grpc_health_probe](https://github.com/grpc-ecosystem/grpc-health-probe).