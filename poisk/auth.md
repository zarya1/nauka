## Auth: Authentication and Authorization
### The main goals when implementing security in distributed services:
- Encrypt data to protect against man-in-the-middle attacks;
- Authenticate to identify clients;
- Authorize to determine the permissions of the identified clients

The first two goals were achieved in this project through mTLS (mutual TLS), the most common method used to secure machine-to-machine communication. Authorization is needed when you have resources in your services with varying levels of ownership. We are implementing an ACL in this project for this security goal.

Here in this project, we haven't focused on third-party mechanisms for generating and managing certificates, so we decided to use a [Cloudflare](https://github.com/cloudflare/cfssl) library to generate self-signed certificates for both client and server. Instructions for installing it are in the [Prerequisite](./prerequisite.md) section. As we achieve authentication and therefore know who the client is, we can use ACL to get authorization. [Casbin](https://github.com/casbin/casbin) is a library we are using for this purpose (dependency is already listed in the go.mod file). You will find in the directory below all the configuration files needed by cfssl and casbin libraries (used by `make gencert` target, as explained in [Building, Testing and Delivering ](./build.md) section):
```
certs-cfg/
├── ca-config.json
├── ca-csr.json
├── client-csr.json
├── model.conf
├── policy.csv
└── server-csr.json
```
Auth setup starts in the source file below:
```
internal/cmd/server/
├── cartserver.go
```
```
85  func (a *CmdServer) setupServer() error {
86	  authorizer := auth.New(
87		a.Config.ACLModelFile,
88		a.Config.ACLPolicyFile,
89	 )
90	 serverConfig := &grpc_protocol.Config{
91		ShoppingCart:  a.cart,
92		Authorizer: authorizer,
93	 }
94	 var opts []grpc.ServerOption
95	 if a.Config.ServerTLSConfig != nil {
96		creds := credentials.NewTLS(a.Config.ServerTLSConfig)
97		opts = append(opts, grpc.Creds(creds))
98	 }
99	 var err error
100	 a.server, err = grpc_protocol.NewGRPCServer(serverConfig, opts...)
101	 if err != nil {
102		return err
103	 }
```
And it continues in the file source below:
```
internal/protocol/grpc/
├── server.go
```
```
80
81	grpcOpts = append(grpcOpts,
82		grpc.StreamInterceptor(
83			grpc_middleware.ChainStreamServer(                              
84				grpc_ctxtags.StreamServerInterceptor(),
85				grpc_zap.StreamServerInterceptor(logger, zapOpts...),
86				grpc_auth.StreamServerInterceptor(authenticate),             <-- interceptor function call for auth pattern
87			)), grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(      
88			grpc_ctxtags.UnaryServerInterceptor(),
89			grpc_zap.UnaryServerInterceptor(logger, zapOpts...),
90			grpc_auth.UnaryServerInterceptor(authenticate),                  <-- interceptor function call for auth pattern
91		)),
92		grpc.StatsHandler(&ocgrpc.ServerHandler{}),
93	)
94	gsrv := grpc.NewServer(grpcOpts...)
95
```