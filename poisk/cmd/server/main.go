package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	cmdsrv "zarya1/poisk/internal/cmd/server"
	"zarya1/poisk/internal/config"
)

var ServiceImplementing = "inmemory"
var GitCommit = "undefined"

func main() {
	fmt.Println("Service implementing: " + ServiceImplementing)
        fmt.Println("Git commit: " + GitCommit)

	cli := &cli{}

	cmd := &cobra.Command{
		Use:     "poisk",
		PreRunE: cli.setupConfig,
		RunE:    cli.run,
	}

	if err := setupFlags(cmd); err != nil {
		log.Fatal(err)
	}

	if err := cmd.Execute(); err != nil {
		log.Fatal(err)
	}
}



type cli struct {
	cfg cfg
}

type cfg struct {
	cmdsrv.Config
	ServerTLSConfig config.TLSConfig
}



func setupFlags(cmd *cobra.Command) error {
	hostname, err := os.Hostname()
	if err != nil {
		log.Fatal(err)
	}

	cmd.Flags().String("config-file", "", "Path to config file.")

	datacfg := "inmem"
	cmd.Flags().String("data-cfg",
		datacfg,
		"Cart data cfg string.")
	cmd.Flags().String("node-name", hostname, "Unique server ID.")

	cmd.Flags().Int("rpc-port",
		8400,
		"Port for RPC clients (and Raft) connections.")

	cmd.Flags().String("acl-model-file", "", "Path to ACL model.")
	cmd.Flags().String("acl-policy-file", "", "Path to ACL policy.")

	cmd.Flags().String("server-tls-cert-file", "", "Path to server tls cert.")
	cmd.Flags().String("server-tls-key-file", "", "Path to server tls key.")
	cmd.Flags().String("server-tls-ca-file",
		"",
		"Path to server certificate authority.")

	return viper.BindPFlags(cmd.Flags())
}


func (c *cli) setupConfig(cmd *cobra.Command, args []string) error {
	var err error

	configFile, err := cmd.Flags().GetString("config-file")
	if err != nil {
		return err
	}
	viper.SetConfigFile(configFile)

	if err = viper.ReadInConfig(); err != nil {
		// it's ok if config file doesn't exist
		if _, ok := err.(viper.ConfigFileNotFoundError); !ok {
			return err
		}
	}

	c.cfg.Datacfg = viper.GetString("data-cfg")
	c.cfg.NodeName = viper.GetString("node-name")
	c.cfg.RPCPort = viper.GetInt("rpc-port")
	c.cfg.ACLModelFile = viper.GetString("acl-mode-file")
	c.cfg.ACLPolicyFile = viper.GetString("acl-policy-file")
	c.cfg.ServerTLSConfig.CertFile = viper.GetString("server-tls-cert-file")
	c.cfg.ServerTLSConfig.KeyFile = viper.GetString("server-tls-key-file")
	c.cfg.ServerTLSConfig.CAFile = viper.GetString("server-tls-ca-file")

	if c.cfg.ServerTLSConfig.CertFile != "" &&
		c.cfg.ServerTLSConfig.KeyFile != "" {
		c.cfg.ServerTLSConfig.Server = true
		c.cfg.Config.ServerTLSConfig, err = config.SetupTLSConfig(
			c.cfg.ServerTLSConfig,
		)
		if err != nil {
			return err
		}
	}

	return nil
}


func (c *cli) run(cmd *cobra.Command, args []string) error {
	var err error
	cmdServer, err := cmdsrv.New(c.cfg.Config)
	if err != nil {
		return err
	}
	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc, syscall.SIGINT, syscall.SIGTERM)
	<-sigc
	return cmdServer.Shutdown()
}

