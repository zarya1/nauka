package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"log"
	"os"
	"strconv"
        "time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	api "zarya1/poisk/api/v1"
	"zarya1/poisk/internal/config"
)

func main() {
	cli := &cli{}

	cmd := &cobra.Command{
		Use:     "poisk",
		PreRunE: cli.setupConfig,
		RunE:    cli.run,
	}

	if err := setupFlags(cmd); err != nil {
		log.Fatal(err)
	}

	if err := cmd.Execute(); err != nil {
		log.Fatal(err)
	}
}

type cli struct {
	cfg cfg
}

type cfg struct {
	PeerTLSConfig *tls.Config
	CertFile      string
	KeyFile       string
	CAFile        string
	Datacfg       string
	RPCPort       int
	NodeName      string
}

func setupFlags(cmd *cobra.Command) error {
	hostname, err := os.Hostname()
	if err != nil {
		log.Fatal(err)
	}

	cmd.Flags().String("config-file", "", "Path to config file.")

	cmd.Flags().String("node-name", hostname, "Unique server ID.")

	cmd.Flags().Int("rpc-port",
		8400,
		"Port for RPC clients (and Raft) connections.")

	cmd.Flags().String("peer-tls-cert-file", "", "Path to peer tls cert.")
	cmd.Flags().String("peer-tls-key-file", "", "Path to peer tls key.")
	cmd.Flags().String("peer-tls-ca-file",
		"",
		"Path to peer certificate authority.")

	return viper.BindPFlags(cmd.Flags())
}

func (c *cli) setupConfig(cmd *cobra.Command, args []string) error {
	var err error

	configFile, err := cmd.Flags().GetString("config-file")
	if err != nil {
		return err
	}
	viper.SetConfigFile(configFile)

	if err = viper.ReadInConfig(); err != nil {
		// it's ok if config file doesn't exist
		if _, ok := err.(viper.ConfigFileNotFoundError); !ok {
			return err
		}
	}

	c.cfg.NodeName = viper.GetString("node-name")
	c.cfg.RPCPort = viper.GetInt("rpc-port")
	c.cfg.CertFile = viper.GetString("peer-tls-cert-file")
	c.cfg.KeyFile = viper.GetString("peer-tls-key-file")
	c.cfg.CAFile = viper.GetString("peer-tls-ca-file")

	if c.cfg.CertFile != "" &&
		c.cfg.KeyFile != "" {
		c.cfg.PeerTLSConfig, err = config.SetupTLSConfig(
			config.TLSConfig{
				CertFile: c.cfg.CertFile,
				KeyFile:  c.cfg.KeyFile,
				CAFile:   c.cfg.CAFile,
			},
		)
		if err != nil {
			return err
		}
	}

	return nil
}

func (c *cli) run(cmd *cobra.Command, args []string) error {

	tlsConfig, err := config.SetupTLSConfig(config.TLSConfig{
		CertFile:      c.cfg.CertFile,
		KeyFile:       c.cfg.KeyFile,
		CAFile:        c.cfg.CAFile,
		Server:        false,
		ServerAddress: "cart-server.shoppingcart",
	})
	if err != nil {
		return err
	}

	tlsCreds := credentials.NewTLS(tlsConfig)
	opts := []grpc.DialOption{grpc.WithTransportCredentials(tlsCreds)}

	conn, err := grpc.Dial(fmt.Sprintf(
		"cart-server.shoppingcart:%d",
		c.cfg.RPCPort,
	), opts...)
	if err != nil {
		log.Fatal(err)
	}

	buyerID := "001"

	//add an new item to the cart
	client := api.NewShoppingCartClient(conn)
	ctx := context.Background()

	for {
	    res, err := client.AddItem(ctx, &api.AddItemRequest{BuyerID: buyerID, Item: getItemsMock()[0]})
            if err != nil {
		log.Fatal(err)
	    }

	    strValue := strconv.FormatBool(res.Ok)
	    if err != nil {
		log.Fatal(err)
	    }
	    fmt.Println("item ", getItemsMock()[0].Sku, "added: ", strValue)

	    //get a cart
	    shopcart, err := client.GetCart(ctx, &api.CartRequest{BuyerID: buyerID})
	    if err != nil {
		log.Fatal(err)
	    }

	    fmt.Println("Items in the Cart:")
	    for _, item := range shopcart.Cart.Items {
		fmt.Println(item.Sku)
	    }

	    //get the total cost
	    totalCostRes, err := client.TotalCost(ctx, &api.TotalCostRequest{BuyerID: buyerID})
	    if err != nil {
		log.Fatal(err)
	    }
	    totalcost := fmt.Sprintf("%.2f", totalCostRes.TotalCost) 
	    fmt.Println("total cost = ", totalcost)

            //remove item
            resRemove, err := client.RemoveItem(ctx, &api.RemoveItemRequest{BuyerID: buyerID, Item: getItemsMock()[0]})
            if err != nil {
                log.Fatal(err)
            }

            strRemoveValue := strconv.FormatBool(resRemove.Ok)
            if err != nil {
                log.Fatal(err)
            }
            fmt.Println("item ", getItemsMock()[0].Sku, "removed: ", strRemoveValue)

	    time.Sleep(3000 * time.Millisecond)
        }
	return nil
}

//---------------
func getCartMock() (*api.Cart) {
	items := getItemsMock()
	var buyer = "001"
	return &api.Cart{
			BuyerID: buyer,
			Items: items,
	}
}

func getItemsMock() ([]*api.Item) {
	return []*api.Item{{
			Sku: "WC-JT-MD-PP",
			Description: "Winter Collection, Jackets, Medium, Purple",
			Price: 0.10,
	Quantity: 1,
	}, {
	Sku: "SE-RS-10-BL",
			Description: "Summer Edition, Running shoes, Size 10, Blue ",
			Price: 0.15,
			Quantity: 1,
	}}
}
//----------------
