package grpc

import (
	"context"
	"time"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_auth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	api "zarya1/poisk/api/v1"

	grpc_zap "github.com/grpc-ecosystem/go-grpc-middleware/logging/zap"
	grpc_ctxtags "github.com/grpc-ecosystem/go-grpc-middleware/tags"

	"go.opencensus.io/plugin/ocgrpc"
	"go.opencensus.io/stats/view"
	"go.opencensus.io/trace"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"


	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/peer"
	"google.golang.org/grpc/status"
        "google.golang.org/grpc/health"

	healthpb "google.golang.org/grpc/health/grpc_health_v1"
)


type Config struct {
	ShoppingCart ShoppingCart
	Authorizer Authorizer
}

const (
	objectWildcard = "*"
	writeAction  = "write"
	readAction  = "read"
)


var _ api.ShoppingCartServer = (*grpcServer)(nil)

type grpcServer struct {
	api.UnimplementedShoppingCartServer
	*Config
}

func newgrpcServer(config *Config) (*grpcServer, error) {
	srv := &grpcServer{
		Config: config,
	}
	return srv, nil
}

func NewGRPCServer(config *Config, grpcOpts ...grpc.ServerOption) (
	*grpc.Server,
	error,
) {
	logger := zap.L().Named("server")
	zapOpts := []grpc_zap.Option{
		grpc_zap.WithDurationField(
			func(duration time.Duration) zapcore.Field {
				return zap.Int64(
					"grpc.time_ns",
					duration.Nanoseconds(),
				)
			},
		),
	}

	trace.ApplyConfig(trace.Config{DefaultSampler: trace.AlwaysSample()})
	err := view.Register(ocgrpc.DefaultServerViews...)
	if err != nil {
		return nil, err
	}

	grpcOpts = append(grpcOpts,
		grpc.StreamInterceptor(
			grpc_middleware.ChainStreamServer(
				grpc_ctxtags.StreamServerInterceptor(),
				grpc_zap.StreamServerInterceptor(logger, zapOpts...),
				grpc_auth.StreamServerInterceptor(authenticate),
			)), grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			grpc_ctxtags.UnaryServerInterceptor(),
			grpc_zap.UnaryServerInterceptor(logger, zapOpts...),
			grpc_auth.UnaryServerInterceptor(authenticate),
		)),
		grpc.StatsHandler(&ocgrpc.ServerHandler{}),
	)
	gsrv := grpc.NewServer(grpcOpts...)

	hsrv := health.NewServer()
        hsrv.SetServingStatus("", healthpb.HealthCheckResponse_SERVING)
        healthpb.RegisterHealthServer(gsrv, hsrv)

	srv, err := newgrpcServer(config)
	if err != nil {
		return nil, err
	}
	api.RegisterShoppingCartServer(gsrv, srv)
	return gsrv, nil
}

func (s *grpcServer) AddItem(ctx context.Context, item *api.AddItemRequest) (
	*api.AddItemResponse, error) {
	if err := s.Authorizer.Authorize(
		subject(ctx),
		objectWildcard,
		writeAction,
	); err != nil {
		return nil, err
	}
	added, err := s.ShoppingCart.AddItem(item)
	if err != nil {
		return nil, err
	}
	return added, nil
}


func (s *grpcServer) RemoveItem(ctx context.Context, item *api.RemoveItemRequest) (
	*api.RemoveItemResponse, error) {
	if err := s.Authorizer.Authorize(
		subject(ctx),
		objectWildcard,
		writeAction,
	); err != nil {
		return nil, err
	}
	removed, err := s.ShoppingCart.RemoveItem(item)
	if err != nil {
		return nil, err
	}
	return removed, nil
}


func (s *grpcServer) GetCart(ctx context.Context, cartRequest *api.CartRequest) (
        *api.CartResponse, error) {
        if err := s.Authorizer.Authorize(
                subject(ctx),
                objectWildcard,
                readAction,
        ); err != nil {
                return nil, err
        }
        cart, err := s.ShoppingCart.GetCart(cartRequest)
        if err != nil {
                return nil, err
        }
	return cart, nil
}

func (s *grpcServer) TotalCost(ctx context.Context, totalCostRequest *api.TotalCostRequest) (
        *api.TotalCostResponse, error) {
        if err := s.Authorizer.Authorize(
                subject(ctx),
                objectWildcard,
                readAction,
        ); err != nil {
                return nil, err
        }
        totalCost, err := s.ShoppingCart.TotalCost(totalCostRequest)
        if err != nil {
                return nil, err
        }
        return totalCost, nil
}


type ShoppingCart interface {
	AddItem(*api.AddItemRequest) (*api.AddItemResponse, error)
	RemoveItem(*api.RemoveItemRequest) (*api.RemoveItemResponse, error)
	GetCart(*api.CartRequest) (*api.CartResponse, error)
	TotalCost(*api.TotalCostRequest) (*api.TotalCostResponse, error)
}


type Authorizer interface {
	Authorize(subject, object, action string) error
}


func authenticate(ctx context.Context) (context.Context, error) {
	peer, ok := peer.FromContext(ctx)
	if !ok {
		return ctx, status.New(
			codes.Unknown,
			"couldn't find peer info",
		).Err()
	}

	if peer.AuthInfo == nil {
		return context.WithValue(ctx, subjectContextKey{}, ""), nil
	}

	tlsInfo := peer.AuthInfo.(credentials.TLSInfo)
	subject := tlsInfo.State.VerifiedChains[0][0].Subject.CommonName
	ctx = context.WithValue(ctx, subjectContextKey{}, subject)

	return ctx, nil
}

func subject(ctx context.Context) string {
	return ctx.Value(subjectContextKey{}).(string)
}

type subjectContextKey struct{}

