package grpc

import (
	// ...
	"context"

	"io/ioutil"
	"net"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/status"

	"flag"

	"go.opencensus.io/examples/exporter"


	api "zarya1/poisk/api/v1"
	"zarya1/poisk/internal/auth"
	"zarya1/poisk/internal/config"
	"zarya1/poisk/internal/cart"
)

// imports...

var debug = flag.Bool("debug", false, "Enable observability for debugging.")

func TestMain(m *testing.M) {
	flag.Parse()
	if *debug {
		logger, err := zap.NewDevelopment()
		if err != nil {
			panic(err)
		}
		zap.ReplaceGlobals(logger)
	}
	os.Exit(m.Run())
}


func TestServer(t *testing.T) {
	for scenario, fn := range map[string]func(
		t *testing.T,
		rootClient api.ShoppingCartClient,
		nobodyClient api.ShoppingCartClient,
		config *Config,
	){
		// ...
		"add a item to the cart succeeeds":    testAddItem,
		"remove a item to the cart succeeeds": testRemoveItem,
		"unauthorized fails":                  testUnauthorized,
	} {
		t.Run(scenario, func(t *testing.T) {
			rootClient,
				nobodyClient,
				config,
				teardown := setupTest(t, nil)
			defer teardown()
			fn(t, rootClient, nobodyClient, config)
		})
	}
}

func setupTest(t *testing.T, fn func(*Config)) (
	rootClient api.ShoppingCartClient,
	nobodyClient api.ShoppingCartClient,
	cfg *Config,
	teardown func(),
) {
	t.Helper()

	l, err := net.Listen("tcp", "127.0.0.1:0")
	require.NoError(t, err)

	newClient := func(crtPath, keyPath string) (
		*grpc.ClientConn,
		api.ShoppingCartClient,
		[]grpc.DialOption,
	) {
		tlsConfig, err := config.SetupTLSConfig(config.TLSConfig{
			CertFile: crtPath,
			KeyFile:  keyPath,
			CAFile:   config.CAFile,
			Server:   false,
		})
		require.NoError(t, err)
		tlsCreds := credentials.NewTLS(tlsConfig)
		opts := []grpc.DialOption{grpc.WithTransportCredentials(tlsCreds)}
		conn, err := grpc.Dial(l.Addr().String(), opts...)
		require.NoError(t, err)
		client := api.NewShoppingCartClient(conn)
		return conn, client, opts
	}

	var rootConn *grpc.ClientConn
	rootConn, rootClient, _ = newClient(
		config.RootClientCertFile,
		config.RootClientKeyFile,
	)

	var nobodyConn *grpc.ClientConn
	nobodyConn, nobodyClient, _ = newClient(
		config.NobodyClientCertFile,
		config.NobodyClientKeyFile,
	)

	serverTLSConfig, err := config.SetupTLSConfig(config.TLSConfig{
		CertFile: config.ServerCertFile,
		KeyFile:  config.ServerKeyFile,
		CAFile:   config.CAFile,
		Server:   true,
	})
	require.NoError(t, err)
	serverCreds := credentials.NewTLS(serverTLSConfig)

	datacfg := "inmem"

	sc, err := cart.NewCart(datacfg)
	require.NoError(t, err)

	authorizer := auth.New(config.ACLModelFile, config.ACLPolicyFile)

	var telemetryExporter *exporter.LogExporter
	if *debug {
		metricsLogFile, err := ioutil.TempFile("", "metrics-*.log")
		require.NoError(t, err)
		t.Logf("metrics log file: %s", metricsLogFile.Name())

		tracesLogFile, err := ioutil.TempFile("", "traces-*.log")
		require.NoError(t, err)
		t.Logf("traces log file: %s", tracesLogFile.Name())

		telemetryExporter, err = exporter.NewLogExporter(exporter.Options{
			MetricsLogFile:    metricsLogFile.Name(),
			TracesLogFile:     tracesLogFile.Name(),
			ReportingInterval: time.Second,
		})
		require.NoError(t, err)
		err = telemetryExporter.Start()
		require.NoError(t, err)
	}

	cfg = &Config{
		ShoppingCart:  sc,
		Authorizer: authorizer,
	}
	if fn != nil {
		fn(cfg)
	}

	server, err := NewGRPCServer(cfg, grpc.Creds(serverCreds))
	require.NoError(t, err)

	go func() {
		server.Serve(l)
	}()

	return rootClient, nobodyClient, cfg, func() {
		server.Stop()
		rootConn.Close()
		nobodyConn.Close()
		l.Close()
		if telemetryExporter != nil {
			time.Sleep(1500 * time.Millisecond)
			telemetryExporter.Stop()
			telemetryExporter.Close()
		}
	}
}

//---
func getItemsMock() ([]*api.Item) {
	return []*api.Item{{
			Sku: "WC-JT-MD-PP",
			Description: "Winter Collection, Jackets, Medium, Purple",
			Price: 0.10,
	Quantity: 1,
	}, {
	Sku: "SE-RS-10-BL",
			Description: "Summer Edition, Running shoes, Size 10, Blue ",
			Price: 0.15,
			Quantity: 1,
	}}
}
//---

func testAddItem(t *testing.T, client, _ api.ShoppingCartClient, config *Config) {
	ctx := context.Background()

	want := &api.AddItemRequest{BuyerID: "001", Item: getItemsMock()[0]}

	added, err := client.AddItem(
		ctx,
		want,
	)
	require.NoError(t, err)
	require.Equal(t, added.Ok, true)
}

func testRemoveItem(t *testing.T, client, _ api.ShoppingCartClient, config *Config) {
	ctx := context.Background()

	want := &api.RemoveItemRequest{BuyerID: "001", Item: getItemsMock()[0]}

	added, err := client.RemoveItem(
		ctx,
		want,
	)
	require.NoError(t, err)
	require.Equal(t, added.Ok, true)
}

func testUnauthorized(
	t *testing.T,
	_,
	client api.ShoppingCartClient,
	config *Config,
) {
	ctx := context.Background()
	added, err := client.AddItem(ctx,
		&api.AddItemRequest{BuyerID: "001", Item: getItemsMock()[0]},
	)
	if added != nil {
		t.Fatalf("add response should be nil")
	}
	gotCode, wantCode := status.Code(err), codes.PermissionDenied
	if gotCode != wantCode {
		t.Fatalf("got code: %d, want: %d", gotCode, wantCode)
	}
	consume, err := client.RemoveItem(ctx,
		&api.RemoveItemRequest{BuyerID: "001", Item: getItemsMock()[0]},
	)
	if consume != nil {
		t.Fatalf("consume response should be nil")
	}
	gotCode, wantCode = status.Code(err), codes.PermissionDenied
	if gotCode != wantCode {
		t.Fatalf("got code: %d, want: %d", gotCode, wantCode)
	}
}
