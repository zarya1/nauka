// +build inmem

package cart

import (
	api "zarya1/poisk/api/v1"
        "log"
)

type Repo struct {
	m map[string]*api.Cart
}

func NewCartRepo() CartRepository {
	var m = map[string]*api.Cart{}
	return &Repo{
		m: m,
	}
}


func (r *Repo) AddItem(item *api.AddItemRequest) (
        *api.AddItemResponse, error) {

        r.m[item.BuyerID] = &api.Cart{BuyerID: item.BuyerID, Items: []*api.Item{item.Item}}
	var added = true

        return &api.AddItemResponse{Ok: added}, nil
}


func (r *Repo) RemoveItem(item *api.RemoveItemRequest) (
        *api.RemoveItemResponse, error) {
        var removed = true

        return &api.RemoveItemResponse{Ok: removed}, nil
}


func (r *Repo) GetCart(req *api.CartRequest) (
        *api.CartResponse, error) {

        if r.m[req.BuyerID] == nil {
                return nil, ErrNotFound
        }

        return &api.CartResponse{Cart: r.m[req.BuyerID]}, nil
}

func (r *Repo) TotalCost(req *api.TotalCostRequest) (
        *api.TotalCostResponse, error) {
        var totalCost float32 = 0.00

	shopcart, err := r.GetCart(&api.CartRequest{BuyerID: req.BuyerID})
	if err != nil {
		log.Fatal(err)
	}

	for _, item := range shopcart.Cart.Items {
		totalCost =+ item.Price
	}

        return &api.TotalCostResponse{TotalCost: totalCost}, nil
}
