package cart

import "errors"

var ErrNotFound = errors.New("Not found")

var ErrCannotBeDeleted = errors.New("Cannot Be Deleted")
