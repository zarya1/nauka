package cart

import (
	api "zarya1/poisk/api/v1"
)

type Cart struct {
        cartRepo CartRepository
}

func NewCart(datacfg string) (*Cart, error) {
        repo := GetRepository()

	cart := &Cart{
                cartRepo: repo,
	}

	return cart, nil
}


func (c *Cart) AddItem(item *api.AddItemRequest) (
        *api.AddItemResponse, error) {

        return c.cartRepo.AddItem(item)
}


func (c *Cart) RemoveItem(item *api.RemoveItemRequest) (
        *api.RemoveItemResponse, error) {

        return c.cartRepo.RemoveItem(item)
}


func (c *Cart) GetCart(req *api.CartRequest) (
        *api.CartResponse, error) {

        return c.cartRepo.GetCart(req)
}

func (c *Cart) TotalCost(req *api.TotalCostRequest) (
        *api.TotalCostResponse, error) {

        return c.cartRepo.TotalCost(req)
}
