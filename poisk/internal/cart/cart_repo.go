package cart

import (
    api "zarya1/poisk/api/v1"
)

type CartRepository interface {
    AddItem(item *api.AddItemRequest) (*api.AddItemResponse, error)
    RemoveItem(item *api.RemoveItemRequest) (*api.RemoveItemResponse, error)
    GetCart(cartRequest *api.CartRequest) (*api.CartResponse, error)
    TotalCost(totalCost *api.TotalCostRequest) (*api.TotalCostResponse, error)
}

func GetRepository() (CartRepository) {
    return NewCartRepo()
}
