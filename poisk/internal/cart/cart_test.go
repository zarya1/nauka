package cart

import (
	"testing"

	"github.com/stretchr/testify/require"
	api "zarya1/poisk/api/v1"
)

func TestCart(t *testing.T) {
	for scenario, fn := range map[string]func(
		t *testing.T, cart *Cart,
	){
		"add a item to the cart succeeds": testAddItem,
		"remove a item to the cart succeeds": testRemoveItem,
	} {
		t.Run(scenario, func(t *testing.T) {
			datacfg := "inmem"

			cart, err := NewCart(datacfg)
			require.NoError(t, err)

			fn(t, cart)
		})
	}
}

//---
func getItemsMock() ([]*api.Item) {
	return []*api.Item{{
			Sku: "WC-JT-MD-PP",
			Description: "Winter Collection, Jackets, Medium, Purple",
			Price: 0.10,
	Quantity: 1,
	}, {
	Sku: "SE-RS-10-BL",
			Description: "Summer Edition, Running shoes, Size 10, Blue ",
			Price: 0.15,
			Quantity: 1,
	}}
}
//---

func testAddItem(t *testing.T, cart *Cart) {
	added, err := cart.AddItem(&api.AddItemRequest{BuyerID: "001", Item: getItemsMock()[0]})
	require.NoError(t, err)
	require.Equal(t, added.Ok, true)
}

func testRemoveItem(t *testing.T, cart *Cart) {
	added, err := cart.RemoveItem(&api.RemoveItemRequest{BuyerID: "001", Item: getItemsMock()[0]})
	require.NoError(t, err)
	require.Equal(t, added.Ok, true)
}

