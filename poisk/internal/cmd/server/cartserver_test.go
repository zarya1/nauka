package server_test

import (
	"context"
	"crypto/tls"
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"github.com/travisjeffery/go-dynaport"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	api "zarya1/poisk/api/v1"
	cmdsrv "zarya1/poisk/internal/cmd/server"
	"zarya1/poisk/internal/config"
)


func TestCmdServer(t *testing.T) {
	serverTLSConfig, err := config.SetupTLSConfig(config.TLSConfig{
		CertFile:      config.ServerCertFile,
		KeyFile:       config.ServerKeyFile,
		CAFile:        config.CAFile,
		Server:        true,
		ServerAddress: "127.0.0.1",
	})
	require.NoError(t, err)

	peerTLSConfig, err := config.SetupTLSConfig(config.TLSConfig{
		CertFile:      config.RootClientCertFile,
		KeyFile:       config.RootClientKeyFile,
		CAFile:        config.CAFile,
		Server:        false,
		ServerAddress: "127.0.0.1",
	})
	require.NoError(t, err)

	ports := dynaport.Get(2)
	rpcPort := ports[1]

	datacfg := "inmem"

	cmdServer, err := cmdsrv.New(cmdsrv.Config{
			NodeName:        "test01",
			RPCPort:         rpcPort,
			Datacfg:         datacfg,
			ACLModelFile:    config.ACLModelFile,
			ACLPolicyFile:   config.ACLPolicyFile,
			ServerTLSConfig: serverTLSConfig,
			PeerTLSConfig:   peerTLSConfig,
	})
	require.NoError(t, err)

	defer func() {
			err := cmdServer.Shutdown()
			require.NoError(t, err)
			require.NoError(t,
				os.RemoveAll(cmdServer.Config.Datacfg),
			)
	}()
	time.Sleep(3 * time.Second)

        testClient := client(t, cmdServer, peerTLSConfig)
        added, err := testClient.AddItem(
                context.Background(),
		        &api.AddItemRequest{BuyerID: "001", Item: getItemsMock()[0]},
        )
        require.NoError(t, err)
	require.Equal(t, added.Ok, true)

}


func client(
	t *testing.T,
	cmdServer *cmdsrv.CmdServer,
	tlsConfig *tls.Config,
) api.ShoppingCartClient {
	tlsCreds := credentials.NewTLS(tlsConfig)
	opts := []grpc.DialOption{grpc.WithTransportCredentials(tlsCreds)}
	rpcAddr, err := cmdServer.Config.RPCAddr()
	require.NoError(t, err)
	conn, err := grpc.Dial(fmt.Sprintf(
		"%s",
		rpcAddr,
	), opts...)
	require.NoError(t, err)
	client := api.NewShoppingCartClient(conn)
	return client
}

//---
func getItemsMock() ([]*api.Item) {
        return []*api.Item{{
                        Sku: "WC-JT-MD-PP",
                        Description: "Winter Collection, Jackets, Medium, Purple",
                        Price: 0.10,
        Quantity: 1,
        }, {
        Sku: "SE-RS-10-BL",
                        Description: "Summer Edition, Running shoes, Size 10, Blue ",
                        Price: 0.15,
                        Quantity: 1,
        }}
}
//---
