package server

import (
	"crypto/tls"
	"fmt"
	"net"
	"sync"

	"go.uber.org/zap"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"zarya1/poisk/internal/auth"
	"zarya1/poisk/internal/cart"
	grpc_protocol "zarya1/poisk/internal/protocol/grpc"
)

type CmdServer struct {
	Config

	cart        *cart.Cart
	server     *grpc.Server

	shutdown     bool
	shutdowns    chan struct{}
	shutdownLock sync.Mutex
}


type Config struct {
	ServerTLSConfig *tls.Config
	PeerTLSConfig   *tls.Config
	Datacfg         string
	RPCPort         int
	NodeName        string
	ACLModelFile    string
	ACLPolicyFile   string
}

func (c Config) RPCAddr() (string, error) {
	return fmt.Sprintf(":%d", c.RPCPort), nil
}


func New(config Config) (*CmdServer, error) {
	a := &CmdServer{
		Config:    config,
		shutdowns: make(chan struct{}),
	}
	setup := []func() error{
		a.setupLogger,
		a.setupCart,
		a.setupServer,
	}
	for _, fn := range setup {
		if err := fn(); err != nil {
			return nil, err
		}
	}
	return a, nil
}


func (a *CmdServer) setupLogger() error {
	logger, err := zap.NewDevelopment()
	if err != nil {
		return err
	}
	zap.ReplaceGlobals(logger)
	return nil
}


func (a *CmdServer) setupCart() error {
	var err error
	a.cart, err = cart.NewCart(
		a.Config.Datacfg,
	)
	return err
}


func (a *CmdServer) setupServer() error {
	authorizer := auth.New(
		a.Config.ACLModelFile,
		a.Config.ACLPolicyFile,
	)
	serverConfig := &grpc_protocol.Config{
		ShoppingCart:  a.cart,
		Authorizer: authorizer,
	}
	var opts []grpc.ServerOption
	if a.Config.ServerTLSConfig != nil {
		creds := credentials.NewTLS(a.Config.ServerTLSConfig)
		opts = append(opts, grpc.Creds(creds))
	}
	var err error
	a.server, err = grpc_protocol.NewGRPCServer(serverConfig, opts...)
	if err != nil {
		return err
	}
	rpcAddr, err := a.RPCAddr()
	if err != nil {
		return err
	}
	ln, err := net.Listen("tcp", rpcAddr)
	if err != nil {
		return err
	}
	go func() {
		if err := a.server.Serve(ln); err != nil {
			_ = a.Shutdown()
		}
	}()
	return err
}

func (a *CmdServer) Shutdown() error {
	a.shutdownLock.Lock()
	defer a.shutdownLock.Unlock()
	if a.shutdown {
		return nil
	}
	a.shutdown = true
	close(a.shutdowns)

	shutdown := []func() error{
		func() error {
			a.server.GracefulStop()
			return nil
		},
	}
	for _, fn := range shutdown {
		if err := fn(); err != nil {
			return err
		}
	}
	return nil
}

