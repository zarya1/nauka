## prerequisite
**To build, test and deploy the sample project follow the instructions below**
### install go
```
wget https://golang.org/dl/go1.16.5.linux-amd64.tar.gz
sudo tar -C /usr/local -xzf go1.16.5.linux-amd64.tar.gz
export PATH=$PATH:/usr/local/go/bin
```
### install grpc libs and tools
```
go get google.golang.org/grpc@v1.32.0
go get google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.0.0
go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.26
wget https://github.com/protocolbuffers/protobuf/releases/download/v3.17.2/protoc-3.17.2-linux-x86_64.zip
sudo unzip protoc-3.17.2-linux-x86_64.zip -d /usr/local/protobuf
```
### install cloudflare ssl certs generator (mTLS, authentication and authorization tests)
```
go get github.com/cloudflare/cfssl/cmd/cfssl@v1.4.1
go get github.com/cloudflare/cfssl/cmd/cfssljson@v1.4.1
```
### set the path:
```
export PATH=$PATH:/usr/local/protobuf/bin
export PATH=$PATH:/home/$USER/go/bin
```
### install buildah (build OCI images - get rid of Docker in your CI pipes)
```
. /etc/os-release
sudo sh -c "echo 'deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/x${ID^}_${VERSION_ID}/ /' > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list"
wget -nv https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable/x${ID^}_${VERSION_ID}/Release.key -O Release.key
sudo apt-key add - < Release.key
sudo apt-get update -qq
sudo apt-get -qq -y install buildah
```
### install helm (manage your k8s deployment similarly a Linux apt or yum repo)
```
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh 
```
### install docker (for local development and test only)
```
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates curl gnupg lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo   "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo usermod -aG docker $USER
```
### install Kind (alongside Docker, Kind provides a local k8s environment)
```
curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.11.0/kind-linux-amd64
chmod +x ./kind
sudo mv ./kind /usr/local/bin
kind create cluster
```
### install kubectl
```
curl -LO https://dl.k8s.io/release/v1.21.0/bin/linux/amd64/kubectl
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
```