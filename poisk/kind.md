## Kind (kubernetes in Docker, for local test)
**When you found yourself in a situation when you needed to run a pod with a container image that you've just built on your laptop, use Kind. It's super-handy, real quick, and 100% disposable.**
### create a Docker image for grpc server app
```
make build-docker
```
### create a Docker image for grpc client app
```
make build-client-docker
```
### load local docker images into cluster nodes
```
kind load docker-image poisk:0.0.1
kind load docker-image poisk:0.0.1-client
```