## Building, Testing and Delivering
**Use make to build, test and delivery** 
### generate certificates (for tests)
```
make gencert
```
##### the command above will create the following files 
```
certs
├── ca.csr
├── ca-key.pem
├── ca.pem
├── client.csr
├── client-key.pem
├── client.pem
├── model.conf
├── nobody-client.csr
├── nobody-client-key.pem
├── nobody-client.pem
├── policy.csv
├── root-client.csr
├── root-client-key.pem
├── root-client.pem
├── server.csr
├── server-key.pem
└── server.pem
```
### create grpc stubs/skeletons
```
make compile-grpc
```
##### the command above will create the stub/skeleton files (*.pb.go) based on the protobuf file (*.proto)
```
api/
└── v1
    ├── shopping_cart_grpc.pb.go
    ├── shopping_cart.pb.go
    └── shopping_cart.proto

```
### build binaries locally
```
make build
```
##### the command above will create the following binaries files
```
bin/
├── cart-client
└── cart-server
```
### run unit tests 
```
make test
```
### create a OCI image for grpc server app (ci pipelines)
```
make build-oci
```
### create a OCI image for grpc client app (ci pipelines)
```
make build-client-oci
```
### create a Docker image for grpc server app (local development and test)
```
make build-docker
```
### create a Docker image for grpc client app (local development and test)
```
make build-client-docker
```