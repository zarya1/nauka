## gRPC benchmarking and load testing tool
It is difficult to perform load testing and benchmarking for gRPC applications using conventional tools as these applications are more or less tied to specific protocols such as HTTP. Therefore, for gRPC, we need custom load testing tools that can test the gRPC server by generating an RPCś virtual load for the server.
GHZ[^1] is a tool that can be used to test and debug services locally and also in automated continuous integration environments for performance regression testing.

Testing using ghz cli:
```
cd load-testing
./ghz --config additem.json
./ghz --config getcart.json
./ghz --config totalcost.json
./ghz --config removeitem.json
```
It will produce output similar to the one below:
```
Summary:
  Count:	2000
  Total:	150.14 ms
  Slowest:	27.40 ms
  Fastest:	0.37 ms
  Average:	3.22 ms
  Requests/sec:	13321.09

Response time histogram:
  0.369 [1]	|
  3.071 [1403]	|∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎
  5.774 [409]	|∎∎∎∎∎∎∎∎∎∎∎∎
  8.477 [103]	|∎∎∎
  11.180 [34]	|∎
  13.883 [0]	|
  16.585 [0]	|
  19.288 [0]	|
  21.991 [17]	|
  24.694 [20]	|∎
  27.397 [13]	|

Latency distribution:
  10 % in 1.01 ms 
  25 % in 1.47 ms 
  50 % in 2.33 ms 
  75 % in 3.41 ms 
  90 % in 5.60 ms 
  95 % in 7.39 ms 
  99 % in 24.10 ms 

Status code distribution:
  [OK]   2000 responses  
```
You can also upload the report to a ghz-web application. First, start the server:
```
./ghz-web --config webconfig.json
```
Create a new project:

---
| ![grpc](./pics/ghz-new-proj.png) |
|:--:|

Then load your test:
```
./ghz --config getcart.json -O json | http POST localhost:8001/api/projects/1/ingest
```
See the report:

---
| ![grpc](./pics/ghz-report.png) |
|:--:|

 [^1]: [gRPC benchmarking and load testing tool](https://ghz.sh/).