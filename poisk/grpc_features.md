## gRPC features used in this project
### middleware (a collection of gRPC interceptors, helpers, and utils)
```
internal/
.
.
.
└── protocol
    └── grpc
        ├── server.go
```
```
80
81	grpcOpts = append(grpcOpts,
82		grpc.StreamInterceptor(
83			grpc_middleware.ChainStreamServer(                               <-- add a streaming interceptor chain for the server
84				grpc_ctxtags.StreamServerInterceptor(),
85				grpc_zap.StreamServerInterceptor(logger, zapOpts...),
86				grpc_auth.StreamServerInterceptor(authenticate),
87			)), grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(      <-- add a unary interceptor chain for the server
88			grpc_ctxtags.UnaryServerInterceptor(),
89			grpc_zap.UnaryServerInterceptor(logger, zapOpts...),
90			grpc_auth.UnaryServerInterceptor(authenticate),
91		)),
92		grpc.StatsHandler(&ocgrpc.ServerHandler{}),
93	)
94	gsrv := grpc.NewServer(grpcOpts...)
95
```

<font size="3">The middleware is used to connect different components to route requests generated by the client to the backend server, running code before and after the gRPC server or client application. It allows you to apply multiple interceptors at the client or server-side as a chain of interceptors, implementing common patterns such as [Authentication and Authorization (auth)](./auth.md), logging [^1], tracing and metrics [^2]. The snippet above is showing how to apply multiple interceptors for both unary and streaming messaging ([gRPC communication patterns](./communication.md)). The snippet below configures how opencensus collects metrics and trace:</font>
```
74
75	trace.ApplyConfig(trace.Config{DefaultSampler: trace.AlwaysSample()})
76	err := view.Register(ocgrpc.DefaultServerViews...)
77	if err != nil {
78		return nil, err
79	}
80
 ```
 <font size="3">You can test the observability (metrics, logging and tracing) using the following command:</font>
 ```
 go test -race ./internal/protocol/grpc/ -tags=inmem -v -debug=true
 ```
 [^1]: [zap from Uber](https://github.com/uber-go/zap).
 [^2]: [opencensus](https://opencensus.io/).